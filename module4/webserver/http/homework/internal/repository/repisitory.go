package repository

import (
	"gitlab.com/zapirus/gokata/module4/webserver/http/homework/internal/model"
)

var (
	data []model.Data
)

type WorkRepos struct {
}

type WorkRepo interface {
	Take() []model.Data
	Appends(model.Data)
}

func (w *WorkRepos) Take() []model.Data {
	return data
}

func (w *WorkRepos) Appends(us model.Data) {
	data = append(data, us)
}
