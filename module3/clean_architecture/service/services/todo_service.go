package serv

import (
	"gitlab.com/zapirus/gokata/module3/clean_architecture/service/models"
	"gitlab.com/zapirus/gokata/module3/clean_architecture/service/repo"
)

type TodoService interface {
	ListTodos() ([]models.Todo, error)
	CreateTodo(title string) error
	CompleteTodo(todo models.Todo) error
	RemoveTodo(todo models.Todo) error
}

type TodoServices struct {
	todoRepo repo.TaskRepository
}

func NewTodoService(todoRepo repo.TaskRepository) *TodoServices {
	return &TodoServices{todoRepo: todoRepo}
}

func (s *TodoServices) ListTodos(d *models.Todo) ([]models.Todo, error) {
	todos, err := s.todoRepo.GetTasks(d)
	if err != nil {
		return nil, err
	}
	return todos, nil
}

func (s *TodoServices) CreateTodo(todo models.Todo) (models.Todo, error) {
	todos, err := s.todoRepo.GetTasks(&models.Todo{})
	if err != nil {
		return todo, err
	}

	todo.ID = len(todos) + 1
	todos = append(todos, todo)

	return todo, nil
}

func (s *TodoServices) CompleteTodo(todo models.Todo) error {
	val, err := s.todoRepo.GetTask(todo.ID)
	if err != nil {
		return err
	}

	val.Status = "completed"
	s.todoRepo.UpdateTask(val)
	if err != nil {
		return err
	}
	return nil
}

func (s *TodoServices) RemoveTodo(todo models.Todo) error {
	val, err := s.todoRepo.GetTask(todo.ID)
	if err != nil {
		return err
	}
	err = s.todoRepo.DeleteTask(val.ID)
	if err != nil {
		return err
	}
	return nil
}
