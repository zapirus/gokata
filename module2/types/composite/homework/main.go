package main

import "fmt"

func main() {
	s := []int{1, 2, 3}
	Append(&s)
	fmt.Println(s)

	sp := []int{1, 2, 3}
	sp = Appends(sp)
	fmt.Println(sp)
}

func Append(s *[]int) {

	*s = append(*s, 4)
}

func Appends(sp []int) []int {
	sp = append(sp, 4)
	return sp
}
