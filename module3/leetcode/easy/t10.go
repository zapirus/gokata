package main

import "fmt"

func main() {

	arr := []int{2, 5, 1, 3, 4, 7}
	fmt.Println(shuffle(arr, 3))

}

func shuffle(nums []int, n int) []int {
	sl := make([]int, len(nums))

	for i := 0; i < n; i++ {
		sl[2*i], sl[2*i+1] = nums[i], nums[n+i]

	}

	return sl

}
