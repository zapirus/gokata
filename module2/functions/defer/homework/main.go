package main

import (
	"errors"
	"fmt"
)

type MergeDictsJob struct {
	Dicts      []map[string]string
	Merged     map[string]string
	IsFinished bool
}

// errors
var (
	errNotEnoughDicts = errors.New("at least 2 dictionaries are required")
	errNilDict        = errors.New("nil dictionary")
)

func New() *MergeDictsJob {
	return &MergeDictsJob{
		Merged: make(map[string]string),
	}
}

func main() {
	fmt.Println(ExecuteMergeDictsJob(&MergeDictsJob{}))
	defer fmt.Println(ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"}, nil}}))
	fmt.Println(ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"b": "c"}, {"c": "d"}}}))
}

func ExecuteMergeDictsJob(job *MergeDictsJob) (*MergeDictsJob, error) {
	ex := New()
	mp := make(map[string]string)
	if len(job.Dicts) < 2 {
		job.IsFinished = true
		return &MergeDictsJob{IsFinished: true}, errNotEnoughDicts
	} else if len(job.Dicts) >= 2 {
		for _, m := range job.Dicts {
			if m == nil {
				ex.IsFinished = true
				return ex, errNilDict
			}
			for d, s2 := range m {

				mp[d] = s2
				ex.Merged = mp
			}
		}
	}
	ex.IsFinished = true
	return ex, nil
}
