package main

// сложность O(n)

//type ListNode struct {
//	Val  int
//	Next *ListNodes
//}

func pairSum(head *ListNode) int {
	var array []int
	for head.Next != nil {
		array = append(array, head.Val)
		head = head.Next
	}
	array = append(array, head.Val)
	sumArray := make([]int, len(array)/2)
	for i := 0; i < len(array)/2; i++ {
		sumArray[i] = array[i]
	}
	i := 0
	for j := len(array) - 1; j >= len(array)/2; j-- {
		sumArray[i] = sumArray[i] + array[j]
		i++
	}
	max := 0
	for _, v := range sumArray {
		if max < v {
			max = v
		}
	}
	return max
}
