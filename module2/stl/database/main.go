package main

import (
	"database/sql"
	"fmt"

	_ "github.com/mattn/go-sqlite3"
)

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	dbase, err := sql.Open("sqlite3", "module2/stl/database/kata.db")
	checkErr(err)
	statement, err := dbase.Prepare("CREATE TABLE IF NOT EXISTS people (id INTEGER PRIMARY KEY, first_name TEXT, last_name TEXT)")
	checkErr(err)

	statement, err = dbase.Prepare("INSERT INTO people (first_name, last_name) VALUES (?, ?) ")
	checkErr(err)
	statement.Exec("Petr", "Petrov")

	rows, err := dbase.Query("SELECT id, first_name, last_name from people")
	var id int
	var first_name, last_name string

	for rows.Next() {
		rows.Scan(&id, &first_name, &last_name)
		fmt.Printf("%d, %s, %s\n", id, first_name, last_name)

	}

}
