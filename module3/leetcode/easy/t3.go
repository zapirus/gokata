package main

import "fmt"

func main() {
	fmt.Println(convertTemperature(36.50))
}

func convertTemperature(celsius float64) []float64 {
	kal := celsius + 273.15
	far := celsius*1.80 + 32.00

	var ss []float64

	ss = append(ss, kal, far)
	return ss

}
