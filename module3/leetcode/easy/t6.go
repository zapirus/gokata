package main

import "fmt"

func main() {
	sl := []string{"gen", "sps"}
	fmt.Println(uniqueMorseRepresentations(sl))

}

func uniqueMorseRepresentations(words []string) int {
	morseMap := []string{
		".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---",
		"-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-",
		"..-", "...-", ".--", "-..-", "-.--", "--..",
	}

	mp := make(map[string]bool)

	for _, word := range words {
		var morse string
		for _, char := range word {
			morse += morseMap[char-'a']
		}
		mp[morse] = true
	}

	return len(mp)
}
