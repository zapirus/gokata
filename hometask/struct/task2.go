package main

import "fmt"

type Students struct {
	name  string
	age   int
	lists Course
}

type Course struct {
	title string
	desc  string
	stars int
}

func Fio(s *Students) {
	if s.name == "Вася" {
		s.age = 50
	}
	fmt.Println(s.name, s.age, s.lists.title, s.lists.desc, s.lists.stars)

}

func main() {
	name := []string{"Петр", "Вася", "Егор"}
	mt := []string{"Математический фак", "ИТ фак", "Юридический фак"}
	desc := []string{"Фак о математике", "Фак об ИТ", "Фак о защите прав чела"}
	stars := []int{3, 4, 5}
	mp := make(map[int]*Students)
	for i := 0; i < len(mt); i++ {
		s := &Students{
			name: name[i],
			age:  20 + i,
			lists: Course{
				title: mt[i],
				desc:  desc[i],
				stars: stars[i],
			},
		}
		mp[i] = s
	}

	for _, students := range mp {
		Fio(students)
	}
}
