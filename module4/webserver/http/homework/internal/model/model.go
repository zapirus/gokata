package model

type Data struct {
	ID     int    `json:"ID"`
	Name   string `json:"Name"`
	Family string `json:"Family"`
	Age    int    `json:"Age"`
}
