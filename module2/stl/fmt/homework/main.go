package main

import "fmt"

func main() {
	fmt.Println(generateSelfStory("Петя", 23, 3344.5))

}

func generateSelfStory(name string, age int, money float64) string {
	return fmt.Sprintf("Имя %s, возраст %d, бабло %v", name, age, money)
}
