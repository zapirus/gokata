package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

//Написать программу,
//которая будет создавать 10 горутин и каждая из них будет выводить на экран свой номер
//через случайный интервал времени от 1 до 5 секунд. Но номера должны выводиться
//в порядке возрастания.
//	Используйте мьютекс для синхронизации.

func main() {
	var wg sync.WaitGroup
	var mx sync.Mutex

	for i := 0; i < 10; i++ {
		wg.Add(1)
		go work(&wg, &mx, i)

	}
	wg.Wait()
	//time.Sleep(20 * time.Second)

}

var x = 0

func work(group *sync.WaitGroup, mutex *sync.Mutex, i int) {
	r := rand.Intn(5)
	mutex.Lock()
	time.Sleep(time.Duration(r) * time.Second)
	x = i
	fmt.Println(x)
	mutex.Unlock()
	group.Done()

}
