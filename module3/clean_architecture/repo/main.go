package main

import (
	"fmt"

	"gitlab.com/zapirus/gokata/module3/clean_architecture/repo/repository"
)

func main() {

	r := repository.NewUserRepository("/home/huawei/src/gitlab.com/zapirus/gokata/module3/clean_architecture/repos/repository/test.json")

	rs := repository.User{
		ID:   4,
		Name: "Petya",
	}
	rss := repository.User{
		ID:   2,
		Name: "Petr",
	}

	r.Save(rs)
	r.Save(rss)

	fmt.Println(r.Find(4))
	fmt.Println(r.FindAll())

}
