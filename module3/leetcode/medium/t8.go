package main

// сложность O(n + m)

func findSmallestSetOfVertices(n int, edges [][]int) []int {
	ing := make([]int, n)

	for _, edge := range edges {
		ing[edge[1]]++
	}
	var result []int

	for i := 0; i < n; i++ {
		if ing[i] == 0 {
			result = append(result, i)
		}
	}

	return result
}
