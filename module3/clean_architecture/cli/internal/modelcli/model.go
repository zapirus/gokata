package modelcli

type UserField struct {
	Id   string `json:"id"`
	Name string `json:"name"`
	Fam  string `json:"fam"`
	Age  string `json:"age"`
}
