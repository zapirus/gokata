package app

import (
	"fmt"

	"gitlab.com/zapirus/gokata/module3/clean_architecture/cli/internal/modelcli"

	"gitlab.com/zapirus/gokata/module3/clean_architecture/cli/internal/repos"

	"gitlab.com/zapirus/gokata/module3/clean_architecture/cli/internal/service"
)

func Choice(value string) string {
	s := repos.NewFileTaskRepos()
	res, _ := service.NewUserService(s)
	switch value {
	case "create":
		var a, b, c, d string
		fmt.Println("Введи данные ")
		fmt.Print("ID: ")
		fmt.Scan(&d)

		fmt.Print("Имя: ")
		fmt.Scan(&a)

		fmt.Print("Фамилия : ")
		fmt.Scan(&b)

		fmt.Print("Возраст : ")
		fmt.Scan(&c)

		rs := modelcli.UserField{
			Id:   d,
			Name: a,
			Fam:  b,
			Age:  c,
		}

		res.Create(rs)
		fmt.Println("Данные добавлены!")

	case "all":
		fmt.Println("Все данные!")
		fmt.Println(res.All())

	case "edit":
		var a string
		fmt.Println("Введи ID пользователя!")
		fmt.Scan(&a)

		res, err := res.Edit(a)
		if err != nil {
			panic(err)
		}
		fmt.Println(res)

	case "delete":
		var a int
		fmt.Println("Введи ID пользователя!")
		fmt.Scan(&a)
		fmt.Println(res.Delete(a))

	case "exit":
		fmt.Println("Пока")
		return "exit"

	}
	return ""

}
