package main

import (
	"flag"
	"log"

	"gitlab.com/zapirus/gokata/module4/webserver/http/homework/internal/config"
	"gitlab.com/zapirus/gokata/module4/webserver/http/homework/internal/handlers"

	"github.com/BurntSushi/toml"
)

var (
	path string
)

func init() {
	flag.StringVar(&path, "configs-path", "/home/huawei/src/gitlab.com/zapirus/gokata/module4/webserver/http/homework/internal/configs/apiserver.toml", "")
}

func main() {
	flag.Parse()
	conf := config.NewConfig()
	_, err := toml.DecodeFile(path, conf)
	if err != nil {
		log.Fatalln(err)
	}
	s := handlers.New(conf)
	s.Run()
}
