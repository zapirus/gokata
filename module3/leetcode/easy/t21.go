package main

import (
	"fmt"
	"strconv"
)

func main() {
	fmt.Println(subtractProductAndSum(234))

}

func subtractProductAndSum(n int) int {
	rs := strconv.Itoa(n)
	res := 1
	res2 := 0
	for _, i2 := range rs {
		fs := string(i2)
		r, _ := strconv.Atoi(fs)
		res = res * r
		res2 = res2 + r
	}

	return res - res2
}
