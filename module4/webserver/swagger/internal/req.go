package internal

import (
	"gitlab.com/zapirus/gokata/module4/webserver/swagger/internal/modelswag"
)

//go:generate swagger generate spec -o /home/huawei/src/gitlab.com/zapirus/gokata/module4/webserver/swagger/public/swagger.json --scan-models

// swagger:route POST /pet/{petID}/uploadImage pet petUploadImageRequest
// Upload an image pet
// responses:
//   200: petUploadImageResponse

// swagger:parameters petUploadImageRequest
type petUploadImageRequest struct {
	// petID of an item
	// Required: true
	// In: path
	PetID string `json:"petID"`

	// Image which should be uploaded
	// Required: true
	// in: formData
	// type: file
	// format: binary
	File string `json:"file"`
}

// swagger:response petUploadImageResponse
type petUploadImageResponse struct {
}

// swagger:route POST /pet pet petAddRequest
// Добавление питомца.
// responses:
//   200: petAddResponse

// swagger:parameters petAddRequest
type petAddRequest struct {
	// in:body
	// required: true
	Body modelswag.Pet
}

// swagger:response petAddResponse
type petAddResponse struct {
	// in:body
	Body modelswag.Pet
}

// swagger:route GET /pet/{petID} pet petGetByIDRequest
// Получение питомца по id.
// responses:
//   200: petGetByIDResponse

// swagger:parameters petGetByIDRequest
type petGetByIDRequest struct {
	// petID of an item
	//
	// In: path
	PetID string `json:"petID"`
}

// swagger:response petGetByIDResponse
type petGetByIDResponse struct {
	// in:body
	Body modelswag.Pet
}

// swagger:route PUT /pet pet petPutByUpdateRequest
// Update an existing pet.
// responses:
//   200: petPutByUpdateResponse

// swagger:parameters petPutByUpdateRequest
type petPutByUpdateRequest struct {
	// in:body
	// required: true
	Body modelswag.Pet
}

// swagger:response petPutByUpdateResponse
type petPutByUpdateResponse struct {
}

// swagger:route GET /pet/findByStatus pet petFindByStatusRequest
// Update an existing pet.
// responses:
//   200: petFindByStatusResponse

// swagger:parameters petFindByStatusRequest
type petFindByStatusRequest struct {
	// in:body
	// required: true
	Body modelswag.Pet
}

// swagger:response petFindByStatusResponse
type petFindByStatusResponse struct {
	//  Description: Pet
	Body []modelswag.Pet
}

// swagger:route POST /pet/{petID} pet petUpdateRequest
// Update a pet int the store with from data.
// responses:
//   200: petUpdateResponse

// swagger:parameters petUpdateRequest
type petUpdateRequest struct {
	// petID of an item
	// Required: true
	// In: path
	PetID string `json:"petID"`

	// in: formData
	Name string `json:"name"`

	// in: formData
	Status string `json:"status"`
}

// swagger:response petUpdateResponse
type petUpdateResponse struct {
}

// swagger:route DELETE /pet/{petID} pet petDeleteRequest
// Delete a pet.
// responses:
//   200: petDeleteResponse

// swagger:parameters petDeleteRequest
type petDeleteRequest struct {
	// petID of an item
	// Required: true
	// In: path
	PetID string `json:"petID"`
}

// swagger:response petDeleteResponse
type petDeleteResponse struct {
}

// swagger:route POST /store/order order orderPlaceRequest
// Place order.
// responses:
//   200: orderPlaceResponse

// swagger:parameters orderPlaceRequest
type orderPlaceRequest struct {
	// in:body
	// required: true
	Body modelswag.Store
}

// swagger:response orderPlaceResponse
type orderPlaceResponse struct {
	// in:body
	Body modelswag.Store
}

// swagger:route GET /store/order/{orderID} order orderGetRequest
// Find purchase order by OrderID.
// responses:
//   200: orderGetResponse

// swagger:parameters orderPlaceRequest
type orderGetRequest struct {
	// petID of an item
	// Required: true
	// In: path
	OrderID string `json:"orderID"`
}

// swagger:response orderGetResponse
type orderGetResponse struct {
	// in:body
	Body modelswag.Store
}

// swagger:route DELETE /store/order/{orderID} order orderDeleteRequest
// Delete purchase order by OrderID.
// responses:
//   200: orderDeleteResponse

// swagger:parameters orderDeleteRequest
type orderDeleteRequest struct {
	// petID of an item
	// Required: true
	// In: path
	OrderID string `json:"orderID"`
}

// swagger:response orderDeleteResponse
type orderDeleteResponse struct {
}

// swagger:route GET /store/inventory order orderPetStatusRequest
// Returns a map  of status  codes to quantities.
// responses:
//   200: orderPetStatusResponse

// swagger:parameters orderPetStatusRequest
type orderPetStatusRequest struct {
}

// swagger:response orderPetStatusResponse
type orderPetStatusResponse struct {
	//  in: Body
	Body map[string]int
}

// swagger:route POST /user/createWithArray User userCreateListRequest
// Create list of users with given input array.
// responses:
//   200: userCreateListResponse

// swagger:parameters userCreateListRequest
type userCreateListRequest struct {
	//  in: body
	Body []modelswag.User
}

// swagger:response userCreateListResponse
type userCreateListResponse struct {
}

// swagger:route POST /user/createArrayArray User userCreateArrayRequest
// Create list of users with given input array.
// responses:
//   200: userCreateArrayResponse

// swagger:parameters userCreateArrayRequest
type userCreateArrayRequest struct {
	//  in: body
	Body []modelswag.User
}

// swagger:response userCreateListResponse
type userCreateArrayResponse struct {
}

// swagger:route GET /user/{username} User userGetRequest
// Get user by username.
// responses:
//   200: userGetResponse

// swagger:parameters userGetRequest
type userGetRequest struct {
	//  in: path
	Username string `json:"username"`
}

// swagger:response userGetResponse
type userGetResponse struct {
	//  in: body
	Body modelswag.User
}

// swagger:route PUT /user/{username} User userUpdateRequest
// Update user by username.
// responses:
//   200: userUpdateResponse

// swagger:parameters userUpdateRequest
type userUpdateRequest struct {
	//  in: body
	Body modelswag.User
}

// swagger:response userUpdateResponse
type userUpdateResponse struct {
}

// swagger:route DELETE /user/{username} User userDeleteRequest
// Delete user by username.
// responses:
//   200: userDeleteResponse

// swagger:parameters userDeleteRequest
type userDeleteRequest struct {
	//  in: path
	Username string `json:"username"`
}

// swagger:response userDeleteResponse
type userDeleteResponse struct {
}

// swagger:route POST /user User userCreateRequest
// Create user.
// responses:
//   200: userDeleteResponse

// swagger:parameters userCreateRequest
type userCreateRequest struct {
	//  in: body
	Body modelswag.User
}

// swagger:response userCreateResponse
type userCreateResponse struct {
}
