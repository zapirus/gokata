package main

import "fmt"

// AirConditioner is the interface that defines the methods for controlling the air conditioner
type AirConditioner interface {
	TurnOn()
	TurnOff()
	SetTemperature(temp int)
}

// RealAirConditioner is the implementation of the air conditioner
type RealAirConditioner struct{}

type AirConditionerAdapter struct {
	airConditioner *RealAirConditioner
}

type AirConditionerProxy struct {
	adapter       *AirConditionerAdapter
	authenticated bool
}

func NewRealAirConditioner() *RealAirConditioner {
	return &RealAirConditioner{}
}

func NewAirConditionerAdapter() *AirConditionerAdapter {
	cond := NewRealAirConditioner()
	return &AirConditionerAdapter{cond}
}

func NewAirConditionerProxy(authenticated bool) *AirConditionerProxy {
	adapter := NewAirConditionerAdapter()
	return &AirConditionerProxy{adapter: adapter, authenticated: authenticated}
}

func (r RealAirConditioner) TurnOn() {
	fmt.Println("Turning on the air conditioner")
}

func (r RealAirConditioner) TurnOff() {
	fmt.Println("Turning off the air conditioner")
}

func (r RealAirConditioner) SetTemperature(temp int) {
	fmt.Println("Setting air conditioner temperature to", temp)
}

func (a AirConditionerAdapter) TurnOn() {
	a.airConditioner.TurnOn()
}

func (a AirConditionerAdapter) TurnOff() {
	a.airConditioner.TurnOff()
}

func (a AirConditionerAdapter) SetTemperature(temp int) {
	a.airConditioner.SetTemperature(temp)
}

func (a AirConditionerProxy) TurnOn() {
	if !a.authenticated {
		fmt.Println("Access denied: authentication required to turn on the air conditioner")
	} else {
		a.adapter.TurnOn()
	}
}

func (a AirConditionerProxy) TurnOff() {
	if !a.authenticated {
		fmt.Println("Access denied: authentication required to turn off the air conditioner")
	} else {
		a.adapter.TurnOff()
	}

}

func (a AirConditionerProxy) SetTemperature(temp int) {
	if !a.authenticated {
		fmt.Println("Access denied: authentication required to set the temperature on the air conditioner")
	} else {
		a.adapter.SetTemperature(temp)
	}
}

func main() {
	airConditioner := NewAirConditionerProxy(false) // without auth
	airConditioner.TurnOn()
	airConditioner.TurnOff()
	airConditioner.SetTemperature(25)

	airConditioner = NewAirConditionerProxy(true) // with auth
	airConditioner.TurnOn()
	airConditioner.TurnOff()
	airConditioner.SetTemperature(25)
}
