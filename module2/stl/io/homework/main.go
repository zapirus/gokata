package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"os"
)

func main() {
	data := new(bytes.Buffer)
	data.WriteString("there is 3pm, but im still alive to write this code snippet\n")
	data.WriteString("чистый код лучше, чем заумный код\n")
	data.WriteString("ваш код станет наследием будущих программистов\n")
	data.WriteString("задумайтесь об этом\n")

	fl, _ := os.Create("module2/stl/io/homework/example.txt")
	defer fl.Close()
	if _, err := io.Copy(fl, data); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	fl, _ = os.Open("module2/stl/io/homework/example.txt")

	defer fl.Close()

	reader := bufio.NewReader(fl)
	for {
		r, err := reader.ReadString('\n')
		if err != nil {
			if err == io.EOF {
				break
			} else {
				fmt.Println(err)
				return
			}
		}
		fmt.Print(r)

	}
}
