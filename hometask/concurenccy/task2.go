package main

import (
	"fmt"
	"sync"
)

// Написать программу, которая будет создавать 2 горутины и каждая из них будет выполнять функцию,
// которая будет складывать два числа. Одна горутина будет складывать 10 и 20, а другая - 30 и 40.
// Результаты сложения нужно выводить на экран. Но результаты должны выводиться в порядке возрастания.
//
//	Используйте мьютекс для синхронизации.
var mx sync.Mutex
var wg sync.WaitGroup

func main() {
	wg.Add(2)
	f1(&wg)
	wg.Wait()
}

func f1(wg *sync.WaitGroup) {
	defer wg.Done()
	mx.Lock()
	fmt.Println(10 + 20)
	mx.Unlock()
	f2(wg)

}

func f2(wg *sync.WaitGroup) {
	defer wg.Done()
	fmt.Println(30 + 40)
}
