package main

import "fmt"

func main() {
	fmt.Println(numJewelsInStones("z", "zxZZzZZZ"))

}

func numJewelsInStones(jewels string, stones string) int {
	ct := 0
	for i := 0; i < len(jewels); i++ {
		for j := 0; j < len(stones); j++ {
			if string(jewels[i]) == string(stones[j]) {
				ct++
			}
		}
	}
	return ct
}
