package service

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"gitlab.com/zapirus/gokata/module4/webserver/http/homework/internal/model"

	"github.com/sirupsen/logrus"

	"github.com/go-chi/chi/v5"

	"gitlab.com/zapirus/gokata/module4/webserver/http/homework/internal/repository"
)

type MethodsService struct {
}

type Service interface {
	Hello() http.HandlerFunc
	GetUsers() http.HandlerFunc
	UploadFile() http.HandlerFunc
	GetNames() http.HandlerFunc
}

func (s *MethodsService) Hello() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		hi := model.Data{
			Name: "Hello, brother!!!",
		}

		indent, err := json.MarshalIndent(hi, " ", " ")
		if err != nil {
			logrus.Printf("Не удалось преаброзовать: %s", err)
			w.WriteHeader(http.StatusConflict)
			return
		}
		w.Write(indent)
	}
}

func (s *MethodsService) GetUsers() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		user := repository.WorkRepos{}
		take := user.Take()
		if r.Method == "GET" {
			w.Header().Set("Content-Type", "application/json")
			res := take
			rs, err := json.MarshalIndent(res, "", " ")
			if err != nil {
				logrus.Printf("Не удалось преаброзовать: %s", err)
				w.WriteHeader(http.StatusConflict)
				return
			}
			w.Write(rs)
		} else if r.Method == "POST" {
			user3 := model.Data{
				ID:     3,
				Name:   "Vasya",
				Family: "Vasilev",
				Age:    40,
			}
			user4 := model.Data{
				ID:     4,
				Name:   "Magomed",
				Family: "Magomedov",
				Age:    20,
			}

			user.Appends(user3)
			user.Appends(user4)
			fmt.Fprintf(w, "*** Новые данные захардкожены ***")
			logrus.Println("Новые данные захардкожены)))")

		}
	}
}

func (s *MethodsService) GetUser() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		user := repository.WorkRepos{}
		res := user.Take()

		id, err := strconv.Atoi(chi.URLParam(r, "id"))
		if err != nil {
			logrus.Printf("Не удалось преаброзовать: %s", err)
			w.WriteHeader(http.StatusConflict)
			return
		}

		for i, user := range res {
			if user.ID == id {
				rs, err := json.MarshalIndent(res[i], "", " ")
				if err != nil {
					logrus.Printf("Не удалось преаброзовать: %s", err)
					w.WriteHeader(http.StatusConflict)
					return
				}
				w.Write(rs)
				return
			}
		}
	}
}

func (s *MethodsService) UploadFile() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		file, header, err := r.FormFile("file")
		if err != nil {
			logrus.Printf("Ошибка. Проверь запрос: %v", err)
			return
		}
		defer file.Close()

		fBytes, err := ioutil.ReadAll(file)
		if err != nil {
			logrus.Printf("Ошибка чтения: %v", err)
			return
		}

		fName := header.Filename
		err = ioutil.WriteFile("/home/huawei/src/gitlab.com/zapirus/gokata/module4/webserver/http/homework/internal/public/"+fName, fBytes, 0644)
		if err != nil {
			logrus.Printf("Ошибка записи: %v", err)
			return
		}

		logrus.Printf("Файл %s успешно записан", fName)
	}
}

func (s *MethodsService) GetNames() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		files, err := ioutil.ReadDir("/home/huawei/src/gitlab.com/zapirus/gokata/module4/webserver/http/homework/internal/public")
		if err != nil {
			logrus.Printf("Ошибка чтения: %s", err)
			return
		}
		var fNames []string
		for _, file := range files {
			fNames = append(fNames, file.Name())
		}
		logrus.Println(fNames)
	}
}
