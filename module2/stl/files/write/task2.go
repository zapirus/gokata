package main

import (
	"bufio"
	"fmt"
	"log"
	"os"

	"github.com/mehanizm/iuliia-go"
)

func main() {
	file, err := os.Open("module2/stl/files/write/example.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	var res string
	scanner := bufio.NewScanner(file)
	// optionally, resize scanner's capacity for lines over 64K, see next example
	for scanner.Scan() {
		res += scanner.Text()
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	fmt.Println(res)
	engText := iuliia.Wikipedia.Translate(res)

	file, err = os.Create("module2/stl/files/write/example.processed.txt")

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer file.Close()
	file.WriteString(engText)
}
