package main

// сложность O(n)

func mergeInBetween(list1 *ListNode, a int, b int, list2 *ListNode) *ListNode {
	var start *ListNode
	out := list1
	for count := 0; count <= b; count++ {
		if count == a-1 {
			start = list1
		}
		list1 = list1.Next
	}
	start.Next = list2
	for list2.Next != nil {
		list2 = list2.Next
	}
	list2.Next = list1
	return out
}
