package main

import "strconv"

// сложность O(n)

func minPartitions(n string) int {
	var sum int
	var max = n[0]
	for i := 0; i < len(n); i++ {
		if n[i] > max {
			max = n[i]
		}
	}
	sum, _ = strconv.Atoi(string(max))
	return sum
}
