package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func joinChannels(chs ...<-chan int) chan int {
	mergedCh := make(chan int)

	go func() {
		wg := &sync.WaitGroup{}

		wg.Add(len(chs))

		for _, ch := range chs {
			go func(ch <-chan int, wg *sync.WaitGroup) {
				defer wg.Done()
				for id := range ch {
					mergedCh <- id
				}
			}(ch, wg)
		}
		go func() {
			wg.Wait()
			close(mergedCh)
		}()
	}()

	return mergedCh
}

func generateData() chan int {
	out := make(chan int, 1000)
	exit := make(chan bool)

	ticker := time.NewTicker(6 * time.Millisecond)

	go func() {
		defer close(out)
		for {
			select {
			case _, ok := <-out:
				if !ok {
					return
				}
			case <-exit:
				fmt.Println("The end")
				return
			case t := <-out:
				fmt.Println(t)

			case out <- rand.Intn(100):
			}
		}
	}()

	time.Sleep(30 * time.Second)
	exit <- true
	ticker.Stop()

	return out
}

func main() {
	rand.Seed(time.Now().UnixNano())
	a := make(chan int)
	b := make(chan int)
	c := make(chan int)

	out := generateData()

	go func() {
		for num := range out {
			a <- num
		}
		close(a)
	}()

	go func() {
		for num := range out {
			b <- num
		}
		close(b)
	}()

	go func() {
		for num := range out {
			c <- num
		}
		close(c)
	}()

	joinChannels(a, b, c)

}
