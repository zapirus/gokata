package main

// сложность O(n)

//type TreeNodes struct {
//	Val   int
//	Left  *TreeNodes
//	Right *TreeNodes
//}

func deepestLeavesSum(root *TreeNode) int {
	c := height(root)
	i := 1

	return sum(root, i, c)
}

func height(root *TreeNode) int {
	if root != nil {
		x := height(root.Left)
		y := height(root.Right)

		if x > y {
			return x + 1
		} else {
			return y + 1
		}
	}
	return 0
}

func sum(root *TreeNode, i, c int) int {
	if root != nil {
		if i == c {
			return root.Val
		}
		x := sum(root.Left, i+1, c)
		y := sum(root.Right, i+1, c)
		return x + y
	}
	return 0
}
