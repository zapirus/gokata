// You can edit this code!
// Click here and start typing.
package repository

import (
	"encoding/json"
	"fmt"
	"os"
)

type Repository interface {
	Save(record interface{}) error
	Find(id int) (interface{}, error)
	FindAll() ([]interface{}, error)
}

type UserRepository struct {
	File string
}

func NewUserRepository(file string) *UserRepository {
	return &UserRepository{
		File: file,
	}
}

type User struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

// Загружать данные в конструкторе

func (r *UserRepository) Save(record interface{}) error {

	val, _ := os.ReadFile(r.File)
	var user []interface{}
	json.Unmarshal(val, &user)

	user = append(user, record)

	marshal, _ := json.MarshalIndent(user, "\t", " ")

	os.WriteFile(r.File, marshal, 0644)

	return nil

}

func (r *UserRepository) Find(id int) (interface{}, error) {

	val, err := os.ReadFile(r.File)
	var user []User
	err = json.Unmarshal(val, &user)
	fmt.Println(err)

	for _, u := range user {
		user = append(user, u)
		if u.ID == id {
			return u, nil
		}
	}
	return user, nil
}

func (r *UserRepository) FindAll() ([]interface{}, error) {
	val, _ := os.ReadFile(r.File)
	var user []interface{}
	json.Unmarshal(val, &user)
	return user, nil

}
