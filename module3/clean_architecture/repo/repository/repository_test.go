package repository

import (
	"encoding/json"
	"os"
	"testing"
)

func TestUserRepository_Save(t *testing.T) {
	file := "/home/huawei/src/gitlab.com/zapirus/gokata/module3/clean_architecture/repos/repository/test.json"
	repo := NewUserRepository(file)

	user := User{
		ID:   1,
		Name: "Petya",
	}

	err := repo.Save(user)

	if err != nil {
		t.Errorf("Failed to save user: %v", err)
	}

	// проверяем, что пользователь был сохранен в файл
	val, err := os.ReadFile(file)
	if err != nil {
		t.Errorf("Failed to read file: %v", err)
	}

	var users []User
	err = json.Unmarshal(val, &users)
	if err != nil {
		t.Errorf("Failed to unmarshal data: %v", err)
	}

	if len(users) != 1 {
		t.Errorf("Expected 1 user, but got %d", len(users))
	}

	if users[0].ID != user.ID {
		t.Errorf("Expected user ID %d, but got %d", user.ID, users[0].ID)
	}

	if users[0].Name != user.Name {
		t.Errorf("Expected user name %s, but got %s", user.Name, users[0].Name)
	}

}

func TestUserRepository_Find(t *testing.T) {
	file := "/home/huawei/src/gitlab.com/zapirus/gokata/module3/clean_architecture/repos/repository/test.json"
	repo := NewUserRepository(file)

	user1 := User{
		ID:   1,
		Name: "Petya",
	}

	user2 := User{
		ID:   2,
		Name: "Petr",
	}

	// сохраняем пользователей
	repo.Save(user1)
	repo.Save(user2)

	// ищем пользователя по ID
	foundUser, err := repo.Find(2)

	if err != nil {
		t.Errorf("Failed to find user: %v", err)
	}

	if foundUser.(User).ID != user2.ID {
		t.Errorf("Expected user ID %d, but got %d", user2.ID, foundUser.(User).ID)
	}

	if foundUser.(User).Name != user2.Name {
		t.Errorf("Expected user name %s, but got %s", user2.Name, foundUser.(User).Name)
	}

}

func TestUserRepository_FindAll(t *testing.T) {
	file := "/home/huawei/src/gitlab.com/zapirus/gokata/module3/clean_architecture/repos/repository/test.json"
	repo := NewUserRepository(file)

	user1 := User{
		ID:   1,
		Name: "John",
	}

	user2 := User{
		ID:   2,
		Name: "Jane",
	}

	// сохраняем пользователей
	repo.Save(user1)
	repo.Save(user2)

	// ищем всех пользователей
	allUsers, err := repo.FindAll()

	if err != nil {
		t.Errorf("Failed to find users: %v", err)
	}

	if len(allUsers) != 2 {
		t.Errorf("Expected 2 users, but got %d", len(allUsers))
	}

}
