package main

import (
	"fmt"
	"unsafe"
)

func main() {
	n := 12345654321
	fmt.Println(unsafe.Sizeof(n))
	var numUint uint = 2 << 3
	fmt.Println("left shift uint", numUint)

}
