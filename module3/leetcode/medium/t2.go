package main

// сложность O(n^2)

func sortTheStudents(sls [][]int, k int) [][]int {
	sl := make([]int, len(sls))
	result := sls
	for i, v := range sls {
		for j, p := range v {
			if j == k {
				sl[i] = p
			}
		}
	}

	for i := range sl {
		for j := 0; j < len(sl)-i-1; j++ {
			if sl[j] < sl[j+1] {
				sl[j], sl[j+1] = sl[j+1], sl[j]
				result[j], result[j+1] = result[j+1], result[j]
			}
		}
	}
	return result
}
