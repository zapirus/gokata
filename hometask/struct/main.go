package main

import "fmt"

type Student struct {
	name  string
	age   int
	grade int
}

func (s Student) infoStudent(sf string) string {
	fmt.Println(sf)
	d := fmt.Sprintf("Имя: %s, Возраст: %d, Класс: %d", s.name, s.age, s.grade)
	return d

}

func main() {
	st := Student{
		name:  "Петр",
		age:   30,
		grade: 11,
	}
	fmt.Println(st.infoStudent("Инфо о студенте"))
}
