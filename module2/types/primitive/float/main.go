package main

import (
	"fmt"
	"unsafe"
)

func main() {
	var uintNum uint32 = 1 << 29
	uintNum += 1 << 28
	uintNum += 1 << 27
	uintNum += 1 << 26
	uintNum += 1 << 25

	uintNum += 1 << 21

	uintNum += 1 << 31

	var floatNum float32
	floatNum = *(*float32)(unsafe.Pointer(&uintNum))
	fmt.Println(floatNum)
	fmt.Println("The end")

}
