package main

import "fmt"

func main() {
	arr := []int{1, 2, 3, 4}

	fmt.Println(runningSum(arr))

}

func runningSum(nums []int) []int {

	val := 0
	var arr []int
	for i, _ := range nums {
		fmt.Println(val)
		val += nums[i]
		arr = append(arr, val)
	}
	return arr
}
