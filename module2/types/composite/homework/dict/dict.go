package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27601,
		},
		{
			Name:  "https://github.com/docker/compose2",
			Stars: 27602,
		},
		{
			Name:  "https://github.com/docker/compose3",
			Stars: 27603,
		},
		{
			Name:  "https://github.com/docker/compose4",
			Stars: 27604,
		},
		{
			Name:  "https://github.com/docker/compose5",
			Stars: 27605,
		},
		{
			Name:  "https://github.com/docker/compose6",
			Stars: 27606,
		},
		{
			Name:  "https://github.com/docker/compose7",
			Stars: 27607,
		},
		{
			Name:  "https://github.com/docker/compose8",
			Stars: 27608,
		},
		{
			Name:  "https://github.com/docker/compose9",
			Stars: 27609,
		},
		{
			Name:  "https://github.com/docker/compose10",
			Stars: 27610,
		},
		{
			Name:  "https://github.com/docker/compose11",
			Stars: 27611,
		},
		{
			Name:  "https://github.com/docker/compose12",
			Stars: 27612,
		},
		{
			Name:  "https://github.com/docker/compose13",
			Stars: 27613,
		},
	}

	dict := make(map[string]interface{})

	for _, project := range projects {
		dict["zapirus"] = project
		fmt.Println(dict["zapirus"])
	}

}
