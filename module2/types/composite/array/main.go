package main

import (
	"fmt"
	"unsafe"
)

func main() {
	rangeArray()
}

func testArray() {
	a := [...]int64{34, 55, 89, 144, 3}

	fmt.Println(unsafe.Sizeof(a))

	fmt.Println(a)

	a[0] = 21
	fmt.Println("change ", a)

	b := &a
	a[0] = 233
	fmt.Println(a)
	fmt.Println(b)

}

type User struct {
	Age    int
	Name   string
	Wallet Wallet
}

type Wallet struct {
	RUR uint64
	USD uint64
	BTC uint64
	ETH uint64
}

func rangeArray() {

	users := [4]User{
		{
			Age:  8,
			Name: "Piter",
			Wallet: Wallet{
				RUR: 13,
				USD: 1,
				BTC: 0,
				ETH: 0,
			},
		},
		{
			Age:  13,
			Name: "Kill",
			Wallet: Wallet{
				RUR: 500,
				USD: 11,
				BTC: 0,
				ETH: 0,
			},
		},
		{
			Age:  21,
			Name: "Allo",
			Wallet: Wallet{
				RUR: 0,
				USD: 111,
				BTC: 1,
				ETH: 3,
			},
		},
		{
			Age:  34,
			Name: "Fitch",
			Wallet: Wallet{
				RUR: 54234,
				USD: 34,
				BTC: 1,
				ETH: 5,
			},
		},
	}

	fmt.Println("ВЫвод тех, кому больше 18")

	for i := range users {
		if users[i].Age > 18 {
			fmt.Println(users[i])
		}

	}

	fmt.Println("Вывод тех, у кого есть бабосики")
	for i, user := range users {
		if users[i].Wallet.BTC > 0 || users[i].Wallet.ETH > 0 {
			fmt.Println(user, i)
		}
	}

}
