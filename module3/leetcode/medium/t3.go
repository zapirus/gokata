package main

//сложность O(n)

//type ListNode struct {
//	Val  int
//	Next *ListNode
//}

func mergeNodes(head *ListNode) *ListNode {
	var nHead *ListNode = head.Next
	var left *ListNode = head.Next
	var right *ListNode = head.Next
	var sum int
	for right != nil {
		if right.Val != 0 {
			sum += right.Val
			right = right.Next
		} else {
			left.Val = sum
			sum = 0
			left.Next = right.Next
			left = right.Next
			right = right.Next
		}
	}
	return nHead
}
