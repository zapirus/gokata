package main

import "fmt"

func main() {
	nums := []int{8, 1, 2, 2, 3}

	fmt.Println(smallerNumbersThanCurrent(nums))

}

func smallerNumbersThanCurrent(nums []int) []int {
	var sl []int

	for i := 0; i < len(nums); i++ {
		val := 0
		for j := 0; j < len(nums); j++ {
			if nums[j] < nums[i] {
				val++
			}
		}
		sl = append(sl, val)
	}

	return sl

}
