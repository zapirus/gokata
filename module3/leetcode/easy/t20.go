package main

func main() {
	candies := []int{12, 1, 12}
	extraCandies := 10

	kidsWithCandies(candies, extraCandies)
}

func kidsWithCandies(candies []int, extraCandies int) []bool {

	maxValue := 0
	res := make([]bool, len(candies))
	for _, i2 := range candies {
		if i2 > maxValue {
			maxValue = i2
		}
	}
	for i, v := range candies {
		if v+extraCandies >= maxValue {
			res[i] = true
		}
	}
	return res

}
