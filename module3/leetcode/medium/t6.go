package main

// сложность O(n log n)
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func constructMaximumBinaryTree(nums []int) *TreeNode {
	if nums == nil || len(nums) == 0 {
		return nil
	}

	return helper(nums, 0, len(nums)-1)
}

func helper(nums []int, left int, right int) *TreeNode {
	if left > right {
		return nil
	}

	var max int
	index := -1
	for i := left; i <= right; i++ {
		if i == left {
			max = nums[i]
			index = i
			continue
		}
		if nums[i] > max {
			max = nums[i]
			index = i
		}
	}

	root := &TreeNode{}
	root.Val = max
	root.Left = helper(nums, left, index-1)
	root.Right = helper(nums, index+1, right)
	return root

}
