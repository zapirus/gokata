package main

import (
	"fmt"
	"math/rand"
)

//Задача 2
/*Создайте структуру Animal, которая содержит поля имя, вид и список характеристик.
Каждая характеристика представляется в виде структуры Characteristic, которая содержит поле название и значение.
Используйте мапу для хранения списка животных.
Напишите функцию для вывода всех животных и их характеристик. */

type Animal struct {
	name           string
	view           string
	specifications Characteristic
}

type Characteristic struct {
	title string
	value int
}

func main() {
	var a int
	mapDog := make(map[int]*Animal)
	dogName := []string{"Бобби", "Балу", "Чаппи", "Тузик"}

	fmt.Scan(&a)

	for i := 0; i < a; i++ {
		rnd := rand.Intn(2)
		rndName := rand.Intn(len(dogName))
		if rnd == 1 {
			user := &Animal{
				name: dogName[rndName],
				view: "Собака",
				specifications: Characteristic{
					title: "Дворняга",
					value: 2 + i,
				},
			}
			mapDog[i] = user
		} else {
			user := &Animal{
				name: dogName[rndName],
				view: "Собака",
				specifications: Characteristic{
					title: "Породистая",
					value: 2 + i,
				},
			}
			mapDog[i] = user
		}
	}

	Info(mapDog)

	for i := 0; i < 333; i++ {
		fmt.Println(i)

	}

}

func Info(m map[int]*Animal) {
	for i, animal := range m {
		fmt.Println(i, animal)
	}
}
