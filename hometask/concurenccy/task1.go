package main

import (
	"fmt"
	"sync"
)

//Написать программу,
//которая будет создавать 10 горутин и каждая из них будет выводить на экран свой номер
//в бесконечном цикле.
//Hо номера должны выводиться в порядке возрастания.
//Используйте мьютекс для синхронизации.

func main() {
	var wg sync.WaitGroup
	wg.Add(10)

	go func() {
		i := 0
		for {
			i++
			fmt.Println("task1", i)

		}
		wg.Done()

	}()

	go func() {
		i := 0
		for {
			i++
			fmt.Println("task2", i)

		}
		wg.Done()

	}()

	go func() {
		i := 0
		for {
			i++
			fmt.Println("task3", i)

		}
		wg.Done()

	}()

	go func() {
		i := 0
		for {
			i++
			fmt.Println("task4", i)

		}
		wg.Done()

	}()

	go func() {
		i := 0
		for {
			i++
			fmt.Println("task5", i)

		}
		wg.Done()

	}()

	go func() {
		i := 0
		for {
			i++
			fmt.Println("task6", i)

		}
		wg.Done()

	}()

	go func() {
		i := 0
		for {
			i++
			fmt.Println("task7", i)

		}
		wg.Done()

	}()

	go func() {
		i := 0
		for {
			i++
			fmt.Println("task8", i)

		}
		wg.Done()

	}()

	go func() {
		i := 0
		for {
			i++
			fmt.Println("task9", i)

		}
		wg.Done()

	}()

	go func() {
		i := 0
		for {
			i++
			fmt.Println("task10", i)

		}
		wg.Done()

	}()
	wg.Wait()
}
