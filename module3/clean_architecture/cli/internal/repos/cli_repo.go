package repos

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"
	"gitlab.com/zapirus/gokata/module3/clean_architecture/cli/internal/modelcli"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "Skar4500"
	dbname   = "kata"
)

type Repos interface {
	Take(field modelcli.UserField)
	GetUserDB() []modelcli.UserField
	EditDB(id string) (string, error)
	DeleteDB(id int) string
}

type FileTaskRepos struct {
}

func NewFileTaskRepos() *FileTaskRepos {
	return &FileTaskRepos{}
}

func (f *FileTaskRepos) Take(field modelcli.UserField) {
	psqlconn := fmt.Sprintf("host=%s port=%d dbname=%s user='%s' password=%s sslmode=disable", host, port, dbname, user, password)

	db, err := sql.Open("postgres", psqlconn)

	defer db.Close()
	if err != nil {
		panic(err)
	}

	insertUser := `insert into "users" ("name", "fam", "age") values($1, $2, $3)`
	_, res := db.Exec(insertUser, field.Name, field.Fam, field.Age)
	if res != nil {
		fmt.Println(res)
	}
}

func (f *FileTaskRepos) GetUserDB() []modelcli.UserField {
	psqlconn := fmt.Sprintf("host=%s port=%d dbname=%s user='%s' password=%s sslmode=disable", host, port, dbname, user, password)
	db, err := sql.Open("postgres", psqlconn)
	if err != nil {
		log.Fatal(err)
	}

	res, err := db.Query("SELECT name, fam, age from users")

	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	var users []modelcli.UserField
	for res.Next() {
		var user modelcli.UserField
		res.Scan(&user.Name, &user.Fam, &user.Age)
		if err != nil {
			log.Fatal(err)
		}
		users = append(users, user)

	}

	return users
}

func (f *FileTaskRepos) EditDB(id string) (string, error) {
	var a, b, c string
	fmt.Println("Введи данные ")

	fmt.Print("Имя: ")
	fmt.Scan(&a)

	fmt.Print("Фамилия: ")
	fmt.Scan(&b)

	fmt.Print("Возраст: ")
	fmt.Scan(&c)
	psqlconn := fmt.Sprintf("host=%s port=%d dbname=%s user='%s' password=%s sslmode=disable", host, port, dbname, user, password)
	db, err := sql.Open("postgres", psqlconn)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_, err = db.Query("UPDATE users SET name= $1, fam = $2, age = $3 WHERE id= $4", a, b, c, id)
	if err != nil {
		panic(err)
	}
	return "Изменено", nil

}

func (f *FileTaskRepos) DeleteDB(id int) string {
	psqlconn := fmt.Sprintf("host=%s port=%d dbname=%s user='%s' password=%s sslmode=disable", host, port, dbname, user, password)
	db, err := sql.Open("postgres", psqlconn)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_, err = db.Query("DELEtE from users where id = $1", id)
	if err != nil {
		panic(err)
	}
	return "Удалено"
}
