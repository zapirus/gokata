module gitlab.com/zapirus/gokata

go 1.20

require (
	github.com/BurntSushi/toml v1.2.1 // indirect
	github.com/blang/semver v3.5.1+incompatible // indirect
	github.com/brianvoe/gofakeit/v6 v6.20.2 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-chi/chi v1.5.4 // indirect
	github.com/go-chi/chi/v5 v5.0.8 // indirect
	github.com/go-swagger/go-swagger v0.30.4 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/lib/pq v1.10.9 // indirect
	github.com/mattn/go-sqlite3 v1.14.16 // indirect
	github.com/mehanizm/iuliia-go v1.0.2 // indirect
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/sirupsen/logrus v1.9.2 // indirect
	github.com/stretchr/testify v1.8.2 // indirect
	github.com/tebeka/selenium v0.9.9 // indirect
	gitlab.com/zapirus/greet v0.0.0-20230324163316-cd26833abbb0 // indirect
	golang.org/x/sync v0.1.0 // indirect
	golang.org/x/sys v0.6.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
