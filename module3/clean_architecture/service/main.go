package main

import (
	"fmt"

	"gitlab.com/zapirus/gokata/module3/clean_architecture/service/models"
	"gitlab.com/zapirus/gokata/module3/clean_architecture/service/repo"
	serv "gitlab.com/zapirus/gokata/module3/clean_architecture/service/services"
)

func main() {

	rs := repo.NewUserRepository("/home/huawei/src/gitlab.com/zapirus/gokata/module3/clean_architecture/service/test.json")

	var createTodoUC = serv.NewTodoService(rs)
	todo := &models.Todo{ID: 1, Title: "zaur", Status: "ok"}
	_, err := createTodoUC.CreateTodo(*todo)
	if err != nil {
		panic(err)
	}
	fmt.Println(rs.GetTasks(todo))

}
