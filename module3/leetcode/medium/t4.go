package main

// сложность O(n)

//type TreeNode struct {
//	Val   int
//	Left  *TreeNode
//	Right *TreeNode
//}

func bstToGst(root *TreeNode) *TreeNode {
	val := 0
	return helper(root, &val)
}

func helper(root *TreeNode, lastVal *int) *TreeNode {
	if root == nil {
		return nil
	}

	right := helper(root.Right, lastVal)
	newRoot := &TreeNode{}
	newRoot.Val = root.Val + *lastVal
	newRoot.Right = right

	*lastVal = newRoot.Val

	left := helper(root.Left, lastVal)
	newRoot.Left = left

	return newRoot

}
