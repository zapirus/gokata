package main

import (
	"fmt"
)

func main() {
	arr := []int{2, 3, 4, 7, 11}
	fmt.Println(findKthPositive(arr, 5))
}

func findKthPositive(arr []int, k int) int {
	i, j := 0, 1
	for k > 0 {
		if i < len(arr) && arr[i] == j {
			i++
		} else {
			k--
		}
		if k == 0 {
			return j
		}
		j++
	}
	return -1
}
