package main

import (
	"fmt"
)

type ParkingSystem struct {
	big    int
	medium int
	small  int
}

func Constructor(big int, medium int, small int) ParkingSystem {

	return ParkingSystem{big: big, medium: medium, small: small}

}

func (this *ParkingSystem) AddCar(carType int) bool {
	val := true

	if carType == 1 {
		val = this.big > 0
		this.big--
	} else if carType == 2 {
		val = this.medium > 0
		this.medium--
	} else if carType == 3 {
		val = this.small > 0
		this.small--
	}

	return val
}

func main() {
	big := 2
	medium := 2
	small := 0
	constructor := Constructor(big, medium, small)
	carType := 4
	val1 := constructor.AddCar(carType)
	val2 := constructor.AddCar(3)
	val3 := constructor.AddCar(4)
	val4 := constructor.AddCar(5)
	fmt.Println(val1, val2, val3, val4)

}
