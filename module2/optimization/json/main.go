package main

import (
	"encoding/json"

	_ "github.com/json-iterator/go"
	jsoniter "github.com/json-iterator/go"
)

type Pets []Pet

func UnmarshalPets(data []byte) (Pets, error) {
	var r Pets
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *Pets) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

func UnmarshalPets2(data []byte) (Pets, error) {
	var r Pets
	err := jsoniter.Unmarshal(data, &r)
	return r, err
}

func (r *Pets) Marshal2() ([]byte, error) {
	return jsoniter.Marshal(r)
}

type Pet struct {
	ID        float64    `json:"id"`
	Category  Category   `json:"category"`
	Name      Name       `json:"name"`
	PhotoUrls []string   `json:"photoUrls"`
	Tags      []Category `json:"tags"`
	Status    Status     `json:"status"`
}

type Category struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

type Name string

const (
	Bird   Name = "bird"
	Doggie Name = "doggie"
	Dogs   Name = "Dogs"
)

type Status string

const (
	Pending Status = "pending"
)
