package main

import (
	"fmt"
	"unsafe"
)

func main() {
	n := 112343211454
	fmt.Println(unsafe.Sizeof(n), "Bytes")

	typeInt()

}

func typeInt() {

	fmt.Println("start type int")
	var numInt int8 = 1 << 1
	fmt.Println("left shift int", numInt, unsafe.Sizeof(numInt))
	numInt = int8(1<<5) - 1
	fmt.Println("left shift int", numInt, unsafe.Sizeof(numInt))
	// 16
	var numInt16 int16 = (1 << 15) - 1
	fmt.Println("left shift int", numInt16, unsafe.Sizeof(numInt16))

	var numInt32 int32 = (1 << 31) - 1
	fmt.Println("left shift int", numInt32, unsafe.Sizeof(numInt32))

	var numInt64 int = (1 << 63) - 1
	fmt.Println("left shift int", numInt64, unsafe.Sizeof(numInt64))

}
