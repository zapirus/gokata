package main

import (
	"fmt"
	"time"
)

func main() {
	msg1 := make(chan string)
	msg2 := make(chan string)

	go func() {
		for {
			time.Sleep(1 * time.Second)
			msg1 <- "ПРошла секунда"
		}
	}()

	go func() {
		for {
			time.Sleep(2 * time.Second)
			msg2 <- "прошло 2 сек"
		}
	}()

	for {
		select {
		case msg := <-msg1:
			fmt.Println(msg)
		case msg := <-msg2:
			fmt.Println(msg)

		}

	}

}
