package main

import (
	"fmt"
	"reflect"
	"strings"
	"time"
)

type Field struct {
	Name string
	Tags map[string]string
}

type UserDTO struct {
	ID            int       `json:"id" db:"id" db_type:"BIGSERIAL_primary_key" db_default:"not_null"`
	Name          string    `json:"name" db:"name" db_type:"varchar(55)" db_default:"default null" db_ops:"create,update"`
	Phone         string    `json:"phone" db:"phone" db_type:"varchar(34)" db_default:"default null" db_index:"index,unique" db_ops:"create,update"`
	Email         string    `json:"email" db:"email" db_type:"varchar(89)" db_default:"default null" db_index:"index,unique" db_ops:"create,update"`
	Password      string    `json:"password" db:"password" db_type:"varchar(144)" db_default:"default null" db_ops:"create,update"`
	Status        int       `json:"status" db:"status" db_type:"int" db_default:"default 0" db_ops:"create,update"`
	Role          int       `json:"role" db:"role" db_type:"int" db_default:"not null" db_ops:"create,update"`
	Verified      bool      `json:"verified" db:"verified" db_type:"boolean" db_default:"not null" db_ops:"create,update"`
	EmailVerified bool      `json:"email_verified" db:"email_verified" db_type:"boolean" db_default:"not null" db_ops:"create,update"`
	PhoneVerified bool      `json:"phone_verified" db:"phone_verified" db_type:"boolean" db_default:"not null" db_ops:"create,update"`
	CreatedAt     time.Time `json:"created_at" db:"created_at" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
	UpdatedAt     time.Time `json:"updated_at" db:"updated_at" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
	DeletedAt     time.Time `json:"deleted_at" db:"deleted_at" db_type:"timestamp" db_default:"default null" db_index:"index"`
}

type EmailVerifyDTO struct {
	ID        int       `json:"id" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null"`
	Email     string    `json:"email" db:"email" db_type:"varchar(89)" db_default:"not null" db_index:"index,unique" db_ops:"create,update"`
	UserID    int       `json:"user_id,omitempty" db:"user_id" db_ops:"create" db_type:"int" db_default:"not null" db_index:"index"`
	Hash      string    `json:"hash,omitempty" db:"hash" db_ops:"create" db_type:"char(36)" db_default:"not null" db_index:"index"`
	Verified  bool      `json:"verified" db:"verified" db_type:"boolean" db_default:"not null" db_ops:"create,update"`
	CreatedAt time.Time `json:"created_at" db:"created_at" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
}

func main() {
	nRef := NewREFLECT()

	nRef.Add(UserDTO{})
	nRef.Add(EmailVerifyDTO{})

}

type REFLECT struct {
	data map[string][]Field
	tgs  map[string]string
}

func NewREFLECT() *REFLECT {
	return &REFLECT{
		data: make(map[string][]Field),
		tgs:  make(map[string]string),
	}
}

func (r REFLECT) Add(v interface{}) {
	t := reflect.TypeOf(v)
	key := t.Name()
	count := t.NumField()

	fields := make([]Field, 0, count)
	for i := 0; i < count; i++ {
		field := t.Field(i)
		for i := 0; i < count; i++ {
			s := t.Field(i).Tag
			ss := strings.Split(string(s), `" `)
			for is := 0; is < len(ss); is++ {
				rf := strings.Split(ss[is], ":")
				sd := strings.Join(rf, " ")
				sgh := strings.Split(sd, `"`)
				r.tgs[sgh[0]] = sgh[1]
			}

		}
		f := Field{
			Name: field.Name,
			Tags: r.tgs,
		}
		fields = append(fields, f)
	}

	r.data[key] = fields

	fmt.Println(r.data)

}
