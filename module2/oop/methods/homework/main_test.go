package main

import (
	"testing"
)

func Test_Sum(t *testing.T) {
	type args struct {
		a float64
		b float64
	}

	tests := []struct {
		name string
		args args
		want float64
	}{
		{
			name: "2+2=4",
			args: args{2, 2},
			want: 4,
		},
		{
			name: "10+10=20",
			args: args{10, 10},
			want: 20,
		},
		{
			name: "250+340=590",
			args: args{250, 340},
			want: 590,
		},
		{
			name: "20+40=60",
			args: args{20, 40},
			want: 60,
		},

		{
			name: "2.5+4.1=6.6",
			args: args{2.5, 4.1},
			want: 6.6,
		},

		{
			name: "2.2+2.2=4.4",
			args: args{2.2, 2.2},
			want: 4.4,
		},

		{
			name: "2.6+2.6=5.2",
			args: args{2.6, 2.6},
			want: 5.2,
		},

		{
			name: "2.0+2.5=4.5",
			args: args{2.0, 2.5},
			want: 4.5,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := sum(tt.args.a, tt.args.b); got != tt.want {
				t.Errorf("sum() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_Average(t *testing.T) {
	type args struct {
		a float64
		b float64
	}

	tests := []struct {
		name string
		args args
		want float64
	}{
		{
			name: "2+2/2=2",
			args: args{2, 2},
			want: 2,
		},
		{
			name: "10+10/2=20",
			args: args{10, 10},
			want: 10,
		},
		{
			name: "250+340/2=295",
			args: args{250, 340},
			want: 295,
		},
		{
			name: "10+10/2=10",
			args: args{10, 10},
			want: 10,
		},

		{
			name: "2.5+4.1/2=3.3",
			args: args{2.5, 4.1},
			want: 3.3,
		},

		{
			name: "2.2+2.2/2=2.2",
			args: args{2.2, 2.2},
			want: 2.2,
		},

		{
			name: "3.0+3.0/2=3.0",
			args: args{3.0, 3.0},
			want: 3.0,
		},

		{
			name: "2.0+2.5/2=2.25",
			args: args{2.0, 2.5},
			want: 2.25,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := average(tt.args.a, tt.args.b); got != tt.want {
				t.Errorf("average() = %v, want %v", got, tt.want)
			}
		})
	}

}

func Test_Divide(t *testing.T) {
	type args struct {
		a float64
		b float64
	}

	tests := []struct {
		name string
		args args
		want float64
	}{
		{
			name: "6/2=3",
			args: args{6, 2},
			want: 3,
		},
		{
			name: "10/2=5",
			args: args{10, 2},
			want: 5,
		},
		{
			name: "20.4 / 3.4=6",
			args: args{20.4, 3.4},
			want: 6,
		},
		{
			name: "40/2=20",
			args: args{40, 2},
			want: 20,
		},

		{
			name: "4.1/2=2.05",
			args: args{4.1, 2},
			want: 2.05,
		},

		{
			name: "5/2=2.5",
			args: args{5, 2},
			want: 2.5,
		},

		{
			name: "3.0/2 = 1.5",
			args: args{3.0, 2},
			want: 1.5,
		},

		{
			name: "2.0/2=1",
			args: args{2.0, 2},
			want: 1,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := divide(tt.args.a, tt.args.b); got != tt.want {
				t.Errorf("average() = %v, want %v", got, tt.want)
			}
		})
	}

}

func Test_Multiply(t *testing.T) {
	type args struct {
		a float64
		b float64
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{
			name: "2*2=4",
			args: args{2, 2},
			want: 4,
		},
		{
			name: "10*2=20",
			args: args{10, 2},
			want: 20,
		},
		{
			name: "20.4 * 3.4=69.36",
			args: args{20.4, 3.4},
			want: 69.36,
		},
		{
			name: "40*2=80",
			args: args{40, 2},
			want: 80,
		},

		{
			name: "4.1*2=8.2",
			args: args{4.1, 2},
			want: 8.2,
		},

		{
			name: "5*2=10",
			args: args{5, 2},
			want: 10,
		},

		{
			name: "3.0*2 = 6",
			args: args{3.0, 2},
			want: 6,
		},

		{
			name: "2.5/2=5",
			args: args{2.5, 2},
			want: 5,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := multiply(tt.args.a, tt.args.b); got != tt.want {
				t.Errorf("average() = %v, want %v", got, tt.want)
			}
		})
	}

}
