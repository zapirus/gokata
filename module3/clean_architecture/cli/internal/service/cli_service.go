package service

import (
	"errors"
	"fmt"

	"gitlab.com/zapirus/gokata/module3/clean_architecture/cli/internal/modelcli"
	"gitlab.com/zapirus/gokata/module3/clean_architecture/cli/internal/repos"
)

type UserService interface {
	Create(user modelcli.UserField)
	All() []modelcli.UserField
	Edit(d string) (string, error)
	Delete(id int) string
}

type Service struct {
	repository repos.Repos
}

func NewUserService(userRepos repos.Repos) (UserService, error) {
	if userRepos == nil {
		return nil, errors.New("empty userRepos")
	}
	return &Service{repository: userRepos}, nil
}
func (s *Service) Create(user modelcli.UserField) {
	fmt.Println(user)
	s.repository.Take(user)

}

func (s *Service) All() []modelcli.UserField {
	return s.repository.GetUserDB()
}

func (s *Service) Edit(id string) (string, error) {
	return s.repository.EditDB(id)

}

func (s *Service) Delete(id int) string {
	res := s.repository.DeleteDB(id)
	return res
}
