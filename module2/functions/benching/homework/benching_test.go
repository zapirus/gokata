package main

import (
	"testing"
)

func BenchmarkSimplestNTimes2(b *testing.B) {
	users2 := genUsers()
	products2 := genProducts()
	users2 = MapUserProducts2(users2, products2)
	for i := 0; i < b.N; i++ {
		MapUserProducts2(users2, products2)
	}
}

func BenchmarkSimplestNTimes1(b *testing.B) {
	users := genUsers()
	products := genProducts()
	users = MapUserProducts(users, products)
	for i := 0; i < b.N; i++ {
		MapUserProducts2(users, products)
	}
}
