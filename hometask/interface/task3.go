package main

import "fmt"

type Shape interface {
	Priam(a, b int) int
	Perim(a, b int) int
}

type St struct {
}

func main() {
	s := St{}
	fmt.Println(s.Priam(1, 3))
	fmt.Println(s.Perim(3, 4))

}

func (s St) Priam(a, b int) int {
	return a * b

}
func (s St) Perim(a, b int) int {
	return (a + b) * 2
}
