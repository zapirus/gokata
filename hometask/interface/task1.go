package main

import (
	"fmt"
	"reflect"
)

func main() {
	SomeFunc(3)
}

func SomeFunc(s interface{}) {
	fmt.Println(reflect.TypeOf(s))

	switch s.(type) {
	case int:
		fmt.Println("INT")

	}
}
