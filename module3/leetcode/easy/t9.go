package main

import "fmt"

func main() {
	arr := []string{"X++", "++X", "--X", "X--"}
	fmt.Println(finalValueAfterOperations(arr))

}

func finalValueAfterOperations(operations []string) int {
	x := 0

	for i, _ := range operations {
		if operations[i] == "--X" || operations[i] == "X--" {
			x--

		} else if operations[i] == "X++" || operations[i] == "X ++" || operations[i] == "++X" {
			x++

		}
	}
	return x
}
