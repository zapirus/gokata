package repository

import (
	"io/ioutil"
	"sync"

	"gitlab.com/zapirus/gokata/module4/webserver/swagger/internal/modelswag"

	"github.com/sirupsen/logrus"
)

type PetStorage struct {
	data               []*modelswag.Pet
	primaryKeyIDx      map[int]*modelswag.Pet
	autoIncrementCount int
	sync.Mutex
}

func NewPetStorage() *PetStorage {
	return &PetStorage{
		data:               make([]*modelswag.Pet, 0, 13),
		primaryKeyIDx:      make(map[int]*modelswag.Pet, 13),
		autoIncrementCount: 1,
	}
}

func (p *PetStorage) Create(pet modelswag.Pet) modelswag.Pet {
	p.Lock()
	defer p.Unlock()
	pet.ID = p.autoIncrementCount
	p.primaryKeyIDx[pet.ID] = &pet
	p.autoIncrementCount++
	p.data = append(p.data, &pet)

	return pet
}

func (p *PetStorage) GetByID(petID int) (modelswag.Pet, error) {
	if v, ok := p.primaryKeyIDx[petID]; ok {
		return *v, nil
	}

	return modelswag.Pet{}, nil

}

func (p *PetStorage) TakeImg(id int, fName string, fBytes []byte) error {
	for _, datum := range p.data {
		if datum.ID == id {
			err := ioutil.WriteFile("/home/huawei/src/gitlab.com/zapirus/gokata/module4/webserver/http/homework/internal/public/"+fName, fBytes, 0644)
			if err != nil {
				logrus.Printf("Ошибка записи: %v", err)
				return nil
			}

			logrus.Printf("Файл %s успешно записан", fName)
			return nil

		}

	}
	logrus.Println("Не фортануло")
	return nil

}

func (p *PetStorage) PutRepo(pet modelswag.Pet) {
	for i, datum := range p.data {
		if datum.ID == pet.ID {
			p.data[i] = &pet
			logrus.Println("Данные обновлены")
			return
		}
	}
	logrus.Println("Не фортануло")

}

func (p *PetStorage) GetStatusRepo(val string) []modelswag.Pet {
	var pets []modelswag.Pet
	for _, datum := range p.data {
		if datum.Status == val {
			pets = append(pets, *datum)
		}
	}
	return pets
}

func (p *PetStorage) UpdatePetRepo(id int, status, name string) {
	for _, datum := range p.data {
		if datum.ID == id {
			datum.Status = status
			datum.Name = name
			logrus.Println("Данные обновлены")
			return
		}

	}
}

func (p *PetStorage) DeletePetRepo(id int) error {
	for i := 0; i < len(p.data); i++ {
		if p.data[i].ID == id {
			p.data = append(p.data[:i], p.data[i+1:]...)
			delete(p.primaryKeyIDx, id)
			logrus.Println("Данные удалены")
			return nil
		}

	}
	return nil
}

//// store data

type OrderStorage struct {
	data               []*modelswag.Store
	primaryKeyIDx      map[int]*modelswag.Store
	autoIncrementCount int
	sync.Mutex
}

func NewOrderStorage() *OrderStorage {
	return &OrderStorage{
		data:               make([]*modelswag.Store, 0, 13),
		primaryKeyIDx:      make(map[int]*modelswag.Store, 13),
		autoIncrementCount: 1,
	}
}

func (p *OrderStorage) CreateOrderRepo(order modelswag.Store) modelswag.Store {
	p.Lock()
	defer p.Unlock()
	order.ID = p.autoIncrementCount
	p.primaryKeyIDx[order.ID] = &order
	p.autoIncrementCount++
	p.data = append(p.data, &order)

	return order
}

func (p *OrderStorage) GetOrderStore(orderID int) (modelswag.Store, error) {
	if v, ok := p.primaryKeyIDx[orderID]; ok {
		return *v, nil
	}
	return modelswag.Store{}, nil
}

func (p *OrderStorage) DeleteOrderRepo(id int) {
	for i := 0; i < len(p.data); i++ {
		if p.data[i].ID == id {
			p.data = append(p.data[:id], p.data[i+1:]...)
			logrus.Println("Данные удалены")
			return
		}
	}
}

func (p *OrderStorage) GetInventoryRepo() map[string]int {
	mp := map[string]int{"available": 0, "pending": 0, "sold": 0}
	for _, datum := range p.data {
		mp[datum.Status]++
	}
	return mp
}

//// user data

type UserStorage struct {
	data               []*modelswag.User
	primaryKeyIDx      map[int]*modelswag.User
	autoIncrementCount int
	sync.Mutex
}

func NewUserStorage() *UserStorage {
	return &UserStorage{
		data:               make([]*modelswag.User, 0, 13),
		primaryKeyIDx:      make(map[int]*modelswag.User, 13),
		autoIncrementCount: 1,
	}
}

func (p *UserStorage) CreateUser(user modelswag.User) modelswag.User {
	p.Lock()
	defer p.Unlock()
	user.ID = p.autoIncrementCount
	p.primaryKeyIDx[user.ID] = &user
	p.autoIncrementCount++
	p.data = append(p.data, &user)

	return user
}

func (p *UserStorage) GetUserRepo(username string) (modelswag.User, error) {
	for _, datum := range p.data {
		if datum.FirstName == username {
			return *datum, nil
		}

	}
	return modelswag.User{}, nil
}

func (p *UserStorage) UpdateUserRepo(name string, user modelswag.User) {
	for i, datum := range p.data {
		if datum.FirstName == name {
			p.data[i] = &user
			logrus.Println("Данные обновлены")
			return
		}

	}
}

func (p *UserStorage) DeleteUserRepo(name string) {
	for i, datum := range p.data {
		if datum.FirstName == name {
			p.data = append(p.data[:i], p.data[i+1:]...)
			logrus.Println("данные удалены")
			return

		}
	}
}
