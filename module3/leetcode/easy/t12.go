package main

import "fmt"

func main() {

	nums := []int{1, 2, 3, 1, 1, 3}
	fmt.Println(numIdenticalPairs(nums))

}

func numIdenticalPairs(nums []int) int {
	ct := 0
	for i := 0; i < len(nums); i++ {
		for j := 0; j < len(nums); j++ {
			if nums[i] == nums[j] && i < j {
				ct += 1
			}
		}
	}
	return ct
}
