package main

import "sort"

// сложность O(q*n*log(n))

func checkArithmeticSubarrays(nums []int, l []int, r []int) []bool {

	isArithmetic := func(arr []int) bool {
		lng := len(arr)
		if lng < 2 {
			return true
		}

		arrC := make([]int, lng)
		copy(arrC, arr)
		sort.Ints(arrC)
		isA := true
		d_0 := arrC[1] - arrC[0]
		for i := 2; i < lng; i++ {
			d_i := arrC[i] - arrC[i-1]
			if d_0 != d_i {
				isA = false
				break
			}
		}
		return isA
	}

	ans := []bool{}
	for i, li := range l {
		ri := r[i]
		ans = append(ans, isArithmetic(nums[li:ri+1]))
	}

	return ans
}
