package repository

import (
	"testing"

	"gitlab.com/zapirus/gokata/module4/webserver/swagger/internal/modelswag"

	"github.com/stretchr/testify/assert"
)

func TestPetStorage_Create(t *testing.T) {
	storage := NewPetStorage()
	pet := modelswag.Pet{Name: "Test Pet"}

	createdPet := storage.Create(pet)

	assert.Equal(t, pet.Name, createdPet.Name)
	assert.NotZero(t, createdPet.ID)
}

func TestPetStorage_GetByID(t *testing.T) {
	storage := NewPetStorage()
	pet := modelswag.Pet{Name: "Test Pet"}
	createdPet := storage.Create(pet)

	foundPet, err := storage.GetByID(createdPet.ID)

	assert.NoError(t, err)
	assert.Equal(t, createdPet, foundPet)
}

func TestPetStorage_TakeImg(t *testing.T) {
	storage := NewPetStorage()
	pet := modelswag.Pet{Name: "Test Pet"}
	createdPet := storage.Create(pet)

	err := storage.TakeImg(createdPet.ID, "test.jpg", []byte("image data"))

	assert.NoError(t, err)
	// Add additional assertions if necessary
}

func TestPetStorage_PutRepo(t *testing.T) {
	storage := NewPetStorage()
	pet := modelswag.Pet{Name: "Test Pet"}
	createdPet := storage.Create(pet)

	updatedPet := modelswag.Pet{ID: createdPet.ID, Name: "Updated Pet"}
	storage.PutRepo(updatedPet)

	foundPet, err := storage.GetByID(createdPet.ID)

	assert.NoError(t, err)
	assert.Equal(t, updatedPet, foundPet)
}

func TestPetStorage_GetStatusRepo(t *testing.T) {
	storage := NewPetStorage()
	pet1 := modelswag.Pet{Name: "Pet 1", Status: "available"}
	pet2 := modelswag.Pet{Name: "Pet 2", Status: "pending"}
	pet3 := modelswag.Pet{Name: "Pet 3", Status: "available"}

	_ = storage.Create(pet1)
	_ = storage.Create(pet2)
	_ = storage.Create(pet3)

	pets := storage.GetStatusRepo("available")

	assert.Len(t, pets, 2)
	assert.Contains(t, pets, pet1)
	assert.Contains(t, pets, pet3)
}

func TestPetStorage_UpdatePetRepo(t *testing.T) {
	storage := NewPetStorage()
	pet := modelswag.Pet{Name: "Test Pet"}
	createdPet := storage.Create(pet)

	storage.UpdatePetRepo(createdPet.ID, "sold", "New Name")

	foundPet, err := storage.GetByID(createdPet.ID)

	assert.NoError(t, err)
	assert.Equal(t, "sold", foundPet.Status)
	assert.Equal(t, "New Name", foundPet.Name)
}

func TestPetStorage_DeletePetRepo(t *testing.T) {
	storage := NewPetStorage()
	pet := modelswag.Pet{Name: "Test Pet"}
	createdPet := storage.Create(pet)

	storage.DeletePetRepo(createdPet.ID)

	_, err := storage.GetByID(createdPet.ID)

	assert.NoError(t, err)
}

func TestOrderStorage_CreateOrderRepo(t *testing.T) {
	storage := NewOrderStorage()
	order := modelswag.Store{ID: 1, Quantity: 2}

	createdOrder := storage.CreateOrderRepo(order)

	assert.Equal(t, order.OrderID, createdOrder.OrderID)
	assert.NotZero(t, createdOrder.ID)
}

func TestOrderStorage_GetOrderStore(t *testing.T) {
	storage := NewOrderStorage()
	order := modelswag.Store{ID: 1, Quantity: 2}
	createdOrder := storage.CreateOrderRepo(order)

	foundOrder, err := storage.GetOrderStore(createdOrder.ID)

	assert.NoError(t, err)
	assert.Equal(t, createdOrder, foundOrder)
}

func TestOrderStorage_DeleteOrderRepo(t *testing.T) {
	storage := NewOrderStorage()
	order := modelswag.Store{ID: 1, Quantity: 2}
	createdOrder := storage.CreateOrderRepo(order)

	storage.DeleteOrderRepo(createdOrder.ID)

	_, err := storage.GetOrderStore(createdOrder.ID)

	assert.NoError(t, err)
}

func TestOrderStorage_GetInventoryRepo(t *testing.T) {
	storage := NewOrderStorage()
	order1 := modelswag.Store{Status: "available"}
	order2 := modelswag.Store{Status: "pending"}
	order3 := modelswag.Store{Status: "available"}

	_ = storage.CreateOrderRepo(order1)
	_ = storage.CreateOrderRepo(order2)
	_ = storage.CreateOrderRepo(order3)

	inventory := storage.GetInventoryRepo()

	assert.Equal(t, 2, inventory["available"])
	assert.Equal(t, 1, inventory["pending"])
}

func TestUserStorage_CreateUser(t *testing.T) {
	storage := NewUserStorage()
	user := modelswag.User{FirstName: "John", LastName: "Doe"}

	createdUser := storage.CreateUser(user)

	assert.Equal(t, user.FirstName, createdUser.FirstName)
	assert.NotZero(t, createdUser.ID)
}

func TestUserStorage_GetUserRepo(t *testing.T) {
	storage := NewUserStorage()
	user := modelswag.User{FirstName: "John", LastName: "Doe"}
	createdUser := storage.CreateUser(user)

	foundUser, err := storage.GetUserRepo(createdUser.FirstName)

	assert.NoError(t, err)
	assert.Equal(t, createdUser, foundUser)
}

func TestUserStorage_UpdateUserRepo(t *testing.T) {
	storage := NewUserStorage()
	user := modelswag.User{FirstName: "John", LastName: "Doe"}
	createdUser := storage.CreateUser(user)

	updatedUser := modelswag.User{ID: createdUser.ID, FirstName: "Jane", LastName: "Smith"}
	storage.UpdateUserRepo(createdUser.FirstName, updatedUser)

	foundUser, err := storage.GetUserRepo(updatedUser.FirstName)

	assert.NoError(t, err)
	assert.Equal(t, updatedUser, foundUser)
}

func TestUserStorage_DeleteUserRepo(t *testing.T) {
	storage := NewUserStorage()
	user := modelswag.User{FirstName: "John", LastName: "Doe"}
	createdUser := storage.CreateUser(user)

	storage.DeleteUserRepo(createdUser.FirstName)

	_, err := storage.GetUserRepo(createdUser.FirstName)

	assert.NoError(t, err)
}
