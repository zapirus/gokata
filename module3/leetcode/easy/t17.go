package main

import (
	"fmt"
	"strings"
)

func main() {
	slc := []string{"alice and bob love leetcode", "i think so too", "this is great thanks very much"}
	fmt.Println(mostWordsFound(slc))

}

func mostWordsFound(sentences []string) int {
	rs := strings.Join(sentences, "\"")
	res := strings.Split(rs, "\"")
	result := 0
	for i := 0; i < len(res); i++ {
		val := strings.Split(res[i], " ")
		if len(val) > result {
			result = len(val)
		}
	}
	return result
}
