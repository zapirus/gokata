package main

import "fmt"

func main() {
	sl := []int{1, 2, 3, 412, 3567, 1222, 35, 0}
	maxElem := 0
	ind := 0
	for i, elem := range sl {
		if elem > maxElem {
			maxElem = elem
			ind = i
		}

	}
	fmt.Println(ind, sl[ind])

	for i := 0; i < len(sl)/2; i++ {

		sl[i], sl[len(sl)-1-i] = sl[len(sl)-1-i], sl[i]

	}
	fmt.Println(sl)
}
