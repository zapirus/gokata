package main

import (
	"fmt"
	"math/rand"
	"sync"
)

//Напишите программу, которая создает 10 горутин, каждая из которых генерирует случайное число
//и записывает его в канал. После того, как все горутины закончат свою работу,
//программа должна считать сумму всех чисел из канала.
//	Используйте WaitGroup для ожидания завершения работы горутин.

func main() {
	var wg sync.WaitGroup
	ch := make(chan int)
	res := 0
	for i := 0; i < 10; i++ {
		wg.Add(1)
		r := rand.Intn(100)
		//fmt.Println(r)
		go func() {
			ch <- r
			defer wg.Done()

		}()
		ct := <-ch
		res += ct

	}
	fmt.Println(res)
	//fmt.Println(<-ch)
	wg.Wait()

}
