package main

import (
	"gitlab.com/zapirus/gokata/module4/webserver/swagger/internal/handlers"
)

func main() {
	handlers.Run()
}
