package main

import (
	"fmt"
)

type PricingStrategy interface {
	Calculate(order *Order) float64
}

type Strategy struct {
	str PricingStrategy
}

type Order struct {
	name  string
	price float64
	count int
}

type RegularPricing struct {
}

func (r *RegularPricing) Calculate(order *Order) float64 {
	res := order.price * float64(order.count)
	return res
}

type SalePricing struct {
	name      string
	salePrice float64
}

func (r *SalePricing) Calculate(order *Order) float64 {
	discount := order.price * r.salePrice / 100
	return (order.price - discount) * float64(order.count)
}

func main() {
	shop := []Order{{"машинка", 600.00, 100}, {"Велосипед", 700.00, 250}, {"Мотоцикл", 900.00, 500}}

	s := []SalePricing{{"обычный заказ", 10}, {"новый клиент", 14}}

	for _, i := range s {
		strategy := Strategy{}
		sum := 0.00
		if i.name == "обычный заказ" {
			strategy.str = &RegularPricing{}
		} else {
			strategy.str = &SalePricing{i.name, i.salePrice}
		}
		for _, s := range shop {
			sum += strategy.str.Calculate(&s)
		}
		if sum < 0 {
			fmt.Println("мы не можем вам дать такую скидку")
			return
		}
		fmt.Printf("Общая стоимость: %s: %v\n", i.name, sum)
	}

}
