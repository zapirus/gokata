package main

import (
	"fmt"
	"sync"
)

//Напишите программу, которая запускает три горутины.
//	Первая горутина записывает числа в канал,
//вторая горутина считывает числа из канала и возводит их в квадрат,
//а третья горутина выводит квадраты чисел на экран.
//	Используйте мьютекс для синхронизации доступа к каналу.

func main() {
	var wg sync.WaitGroup

	ch := make(chan int)
	ch2 := make(chan int)
	ch3 := make(chan int)
	for i := 0; i < 114; i++ {
		wg.Add(3)
		go func() {
			ch <- 2
			ch2 <- 4
			defer wg.Done()

		}()

		go func() {
			rs := <-ch
			rs2 := <-ch2
			defer wg.Done()
			ch3 <- rs * rs2
		}()

		go func() {
			defer wg.Done()
			fmt.Println(<-ch3)
		}()
	}
	wg.Wait()

}
