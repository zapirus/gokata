package main

import "fmt"

func main() {
	arr := [][]int{{1, 5}, {7, 3}, {3, 5}}

	fmt.Println(maximumWealth(arr))

}

func maximumWealth(accounts [][]int) int {
	res := 0
	var nSl []int

	for i := 0; i < len(accounts); i++ {
		rs := accounts[i]
		sum := 0
		for _, r := range rs {
			sum += r
		}
		nSl = append(nSl, sum)
	}

	for _, i2 := range nSl {
		if i2 > res {
			res = i2
		}
	}
	return res

}
