package main

import (
	"fmt"
)

func main() {
	x, y := Pop()
	fmt.Println(x, y)
	a, b := Shift()
	fmt.Println(a, b)
}

func Pop() (int, []int) {
	sl := []int{1, 2, 3, 4, 5}
	return sl[len(sl)-1], sl[:len(sl)-1]
}

func Shift() (int, []int) {
	sl := []int{1, 2, 3, 4, 5}
	return sl[0], sl[1:]
}
