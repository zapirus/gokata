package main

import (
	"fmt"
)

func main() {

	var numberInt int
	numberInt = 4

	var numFloat float32
	numFloat = float32(numberInt)
	fmt.Printf("%T \t %v", numFloat, numFloat)
}
