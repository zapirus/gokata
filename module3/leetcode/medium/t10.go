package main

// сложность O(n!)

func numTilePossibilities(tiles string) int {

	getLettersCounter := func() map[rune]int {
		res := make(map[rune]int)
		for _, v := range tiles {
			res[v] += 1
		}
		return res
	}

	letterCounter := getLettersCounter()
	result := 0
	n := len(tiles)

	var rs func(pos int)
	rs = func(pos int) {
		if pos > 0 {
			result += 1
			if pos == n {
				return
			}
		}
		for k, v := range letterCounter {
			if v <= 0 {
				continue
			}
			letterCounter[k] -= 1
			rs(pos + 1)
			letterCounter[k] += 1
		}
	}

	rs(0)
	return result
}
