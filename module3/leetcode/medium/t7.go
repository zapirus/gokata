package main

// сложность O(n log n)

func InOrder(root *TreeNode, arr *[]int) {
	if root == nil {
		return
	}
	InOrder(root.Left, arr)
	*arr = append(*arr, root.Val)
	InOrder(root.Right, arr)

}

func createBalancedBST(A []int, l, r int) *TreeNode {
	if l > r {
		return nil
	}
	m := (l + r) / 2
	root := &TreeNode{Val: A[m]}
	root.Left = createBalancedBST(A, l, m-1)
	root.Right = createBalancedBST(A, m+1, r)
	return root
}

func balanceBST(root *TreeNode) *TreeNode {
	var arr []int
	InOrder(root, &arr)
	result := createBalancedBST(arr, 0, len(arr)-1)
	return result
}
