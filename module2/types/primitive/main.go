package main

import (
	"fmt"
	"unsafe"
)

func main() {
	n := 12343212435

	fmt.Println(unsafe.Sizeof(n))
}
