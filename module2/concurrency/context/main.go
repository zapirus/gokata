package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"time"
)

const (
	Value = 0
)

func main() {
	http.HandleFunc("/", handle)
	log.Fatal(http.ListenAndServe(":8080", nil))

}

func handle(w http.ResponseWriter, r *http.Request) {

	id := r.Header.Get("UserId")

	ctx := context.WithValue(r.Context(), Value, id)
	res := SomeFunc(ctx)
	w.Write([]byte(res))

}

func SomeFunc(ctx context.Context) string {
	id := ctx.Value(Value)
	select {
	case <-time.After(2 * time.Second):
		return fmt.Sprint("Ok", id)
	case <-ctx.Done():
		log.Print("cancel")
		return ""

	}
}
