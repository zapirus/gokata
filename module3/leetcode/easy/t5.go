package main

import "fmt"

func main() {
	fmt.Println(numberOfMatches(7))

}

func numberOfMatches(n int) int {
	if n%2 == 0 {
		return n / 2
	} else {
		return (n-1)/2 + 1
	}
}
