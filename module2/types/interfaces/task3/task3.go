package main

import (
	"fmt"
)

type User struct {
	ID   int
	Name string
}

func (u *User) GetName() string {
	return u.Name
}

type Userer interface {
	GetName() string
}

func main() {
	u := []User{
		{
			ID:   34,
			Name: "Annet",
		},
		{
			ID:   55,
			Name: "John",
		},
		{
			ID:   89,
			Name: "Alex",
		},
	}
	user(u)
}

func user(users []User) []Userer {

	var value []Userer
	for _, i2 := range users {
		res := append(value, &i2)
		testUserName(res)
	}
	return value

}

func testUserName(users []Userer) {
	for _, u := range users {
		fmt.Println(u.GetName())
	}
}
