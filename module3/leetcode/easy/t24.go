package main

import (
	"fmt"
)

func main() {

	ms := []int{1, 2, 3}

	val := 1
	fmt.Println(decode(ms, val))

}

func decode(encoded []int, first int) []int {
	resOut := make([]int, len(encoded)+1)
	resOut[0] = first

	for i, v := range encoded {
		resOut[i+1] = resOut[i] ^ v
	}

	return resOut
}
