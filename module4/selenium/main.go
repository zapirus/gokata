package main

import (
	"fmt"
	"log"
	"time"

	"github.com/tebeka/selenium"
	"github.com/tebeka/selenium/chrome"
)

const maxTries = 5

func main() {
	// прописываем конфигурацию для драйвера
	caps := selenium.Capabilities{
		"browserName": "chrome",
	}

	// добавляем в конфигурацию драйвера настройки для chrome
	chrCaps := chrome.Capabilities{
		W3C: true,
	}
	caps.AddChrome(chrCaps)

	// переменная нашего веб драйвера
	var wd selenium.WebDriver
	var err error
	// прописываем адрес нашего драйвера
	urlPrefix := selenium.DefaultURLPrefix
	// немного костылей чтобы драйвер не падал
	i := 1
	for i < maxTries {
		wd, err = selenium.NewRemote(caps, urlPrefix)
		if err != nil {
			log.Println(err)
			i++
			continue
		}
		break
	}
	// после окончания программы завершаем работу драйвера
	defer wd.Quit()

	// сразу обращаемся к странице с поиском вакансии по запросу
	page := 1         // номер страницы
	query := "golang" // запрос
	wd.Get(fmt.Sprintf("https://career.habr.com/vacancies?page=%d&q=%s&type=all", page, query))
	elem, err := wd.FindElement(selenium.ByCSSSelector, ".search-total")
	vacCountRaw, err := elem.Text()
	if err != nil {
		panic(err)
	}
	fmt.Println(vacCountRaw)

	elems, err := wd.FindElements(selenium.ByCSSSelector, ".vacancy-card__title-link")
	var links []string
	urlHabr := "https://career.habr.com"
	for el := range elems {
		var link string
		link, err := elems[el].GetAttribute("href")
		if err != nil {
			continue
		}
		links = append(links, urlHabr+link)

	}
	fmt.Println(links)

	// ждем 60 секунд, чтобы успеть посмотреть результат
	time.Sleep(60 * time.Second)
}
