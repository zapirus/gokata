package main

import (
	"fmt"

	"gitlab.com/zapirus/gokata/module3/clean_architecture/cli/internal/app"
)

func main() {
	check := []string{
		"Выберите из списка ниже, что хотите сделать: ",
		"Создать задачу - create",
		"Показать список задач - all",
		"Редактировать задачу - edit",
		"Удалить задачу - delete",
		"Завершить интерактив - exit",
	}
	for {

		for _, s := range check {
			fmt.Println(s)

		}

		var val string
		fmt.Scan(&val)

		r := app.Choice(val)
		if r == "exit" {
			break
		}
	}
}
