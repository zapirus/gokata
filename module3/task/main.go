package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"time"
)

type DoubleLinkedList struct {
	head *Node // начальный элемент в списке
	tail *Node // последний элемент в списке
	curr *Node // текущий элемент меняется при использовании методов next, prev
	len  int   // количество элементов в списке
}

type LinkedLister interface {
	Len() int
	Current() *Node
	Next() *Node
	Prev() *Node
	LoadData(path string) error
}

// LoadData загрузка данных из подготовленного json файла
func (d *DoubleLinkedList) LoadData(path string) error {
	// отсортировать список используя самописный QuickSort
	data, err := ioutil.ReadFile(path)
	if err != nil {
		log.Print(err)
	}
	var comm []Commit
	err = json.Unmarshal(data, &comm)
	if err != nil {
		return err
	}

	json.Unmarshal(data, &comm)

	// отсортировать список используя самописный QuickSort
	for i, num := range comm {
		d.Insert(i, num)
	}

	d.head = quickSort(d.head)
	d.tail = d.head
	for d.tail != nil && d.tail.next != nil {
		d.tail = d.tail.next
	}
	return nil

}

func quickSort(head *Node) *Node {
	if head == nil || head.next == nil {
		return head
	}

	res1 := head
	res2 := head.next

	var val1, val2 *Node
	for res2 != nil {
		next := res2.next
		if res2.data.UUID < res1.data.UUID {
			res2.next = val1
			val1 = res2
		} else {
			res2.next = val2
			val2 = res2
		}
		res2 = next
	}

	val1 = quickSort(val1)
	val2 = quickSort(val2)

	res1.next = val2
	if val1 == nil {
		return res1
	}

	tail := val1
	for tail.next != nil {
		tail = tail.next
	}
	tail.next = res1
	return val1
}

// Len получение длины списка
func (d *DoubleLinkedList) Len() int {
	return d.len
}

// Current получение текущего элемента
func (d *DoubleLinkedList) Current() *Node {
	return d.curr
}

// Next получение следующего элемента
func (d *DoubleLinkedList) Next() *Node {
	if d.curr == nil {
		return nil
	}
	d.curr = d.Current().next
	return d.curr

}

// Prev получение предыдущего элемента
func (d *DoubleLinkedList) Prev() *Node {
	if d.curr == nil || d.curr == d.head {
		return nil
	}
	return d.curr.prev
}

// Insert вставка элемента после n элемента
func (d *DoubleLinkedList) Insert(n int, c Commit) error {

	newNode := &Node{data: &c}
	if n == 0 {
		newNode.next = d.head
		if d.head != nil {
			d.head.prev = newNode
		}
		d.head = newNode
		if d.tail == nil {
			d.tail = newNode
		}
	} else if n == d.len {
		newNode.prev = d.tail
		if d.tail != nil {
			d.tail.next = newNode
		}
		d.tail = newNode
		if d.head == nil {
			d.head = newNode
		}
	} else {
		currNode := d.head
		for i := 0; i < n-1; i++ {
			currNode = currNode.next
		}
		newNode.next = currNode.next
		newNode.prev = currNode
		currNode.next.prev = newNode
		currNode.next = newNode
	}
	d.len++
	return nil

}

// Delete удаление n элемента
func (d *DoubleLinkedList) Delete(n int) error {

	if n == 1 {
		d.head = d.head.next
		if d.head != nil {
			d.head.prev = nil
		}
	} else {
		curr := d.head
		for i := 1; i < n; i++ {
			curr = curr.next
		}
		curr.prev.next = curr.next
		if curr.next != nil {
			curr.next.prev = curr.prev
		}
	}
	d.len--
	return nil

}

// DeleteCurrent удаление текущего элемента
func (d *DoubleLinkedList) DeleteCurrent() error {
	if d.curr == nil {
		return fmt.Errorf("Не фортануло")
	}
	if d.curr.prev == nil {
		d.head = d.curr.next
		if d.head != nil {
			d.head.prev = nil
		}
	} else {
		d.curr.prev.next = d.curr.next
		if d.curr.next != nil {
			d.curr.next.prev = d.curr.prev
		}
	}
	d.curr = d.curr.next
	d.len--
	return nil
}

// Index получение индекса текущего элемента
func (d *DoubleLinkedList) Index() (int, error) {
	if d.curr == nil {
		return -1, fmt.Errorf("Не фортануло")
	}
	ind := 1
	curr := d.head
	for curr != d.curr {
		curr = curr.next
		ind++
	}
	return ind, nil
}

// Pop Операция Pop
func (d *DoubleLinkedList) Pop() *Node {
	if d.len == 0 {
		return nil
	}
	r := d.head
	for r.next != nil {
		r = r.next
	}
	if r.prev != nil {
		r.prev.next = nil
	} else {
		d.head = nil
	}
	d.len--
	return r
}

// Shift операция shift
func (d *DoubleLinkedList) Shift() *Node {
	res := d.head
	d.curr = d.head
	err := d.DeleteCurrent()
	if err != nil {
		panic(err)
	}
	return res

}

// SearchUUID поиск коммита по uuid
func (d *DoubleLinkedList) SearchUUID(uuID string) *Node {
	currNode := d.head
	for _, uuid := range currNode.data.UUID {
		if string(uuid) == uuID {
			return currNode

		}
	}
	return nil
}

// Search поиск коммита по message
func (d *DoubleLinkedList) Search(message string) *Node {
	currNode := d.head
	for _, msg := range currNode.data.Message {
		if string(msg) == message {
			return currNode

		}
	}
	return nil
}

// Reverse возвращает перевернутый список
func (d *DoubleLinkedList) Reverse() *DoubleLinkedList {
	if d.head == nil || d.head.next == nil {
		return d
	}

	curr := d.head
	var next, prev *Node
	for curr != nil {
		next = curr.next
		curr.next = prev
		curr.prev = next
		prev = curr
		curr = next
	}

	d.tail = d.head
	d.head = prev

	return d

}

type Node struct {
	data *Commit
	prev *Node
	next *Node
}

type Commit struct {
	Message string    `json:"message"`
	UUID    string    `json:"uuid"`
	Date    time.Time `json:"date"`
}

//
//func GenerateJSON() {
//	// Дополнительное задание написать генератор данных
//	// используя библиотеку gofakeit
//}

func main() {

	url := "/home/huawei/src/gitlab.com/zapirus/gokata/module3/task/test.json"

	res := &DoubleLinkedList{}
	err := res.LoadData(url)
	if err != nil {
		panic(err)

	}

	fmt.Println(res.Prev())
	fmt.Println(res.Prev())
	fmt.Println(res.Current())
	fmt.Println(res.Prev())
	fmt.Println(res.Prev())
	r := res.Pop()
	fmt.Println(r.data)

	r2 := res.Shift()
	fmt.Println(r2.data)

	rs := res.Reverse()
	fmt.Println(rs.Current())

}
