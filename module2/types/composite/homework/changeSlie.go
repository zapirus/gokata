package main

import (
	"fmt"
)

type Users struct {
	Name string
	Age  int
}

func main() {
	users := []Users{
		{
			Name: "Eva",
			Age:  13,
		},
		{
			Name: "Victor",
			Age:  28,
		},
		{
			Name: "Dex",
			Age:  34,
		},
		{
			Name: "Billy",
			Age:  21,
		},
		{
			Name: "Foster",
			Age:  29,
		},
	}

	subUsers := users[2:len(users)]
	s := editSecondSlice(subUsers)
	fmt.Println(s)
	fmt.Println(users)
}

func editSecondSlice(users []Users) []Users {
	newUsers := make([]Users, 3)
	copy(newUsers, users)
	for i := range users {
		newUsers[i].Name = "unknown"
		newUsers = append(newUsers, users[i])
	}
	return newUsers[0:3]
}
