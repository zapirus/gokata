package main

import "testing"

var jsonData = `

[
      {
    "id": 9223372036854773000,
    "category": {
      "id": 606,
      "name": "string"
    },
    "name": "bird",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 606,
        "name": "Kesha"
      }
    ],
    "status": "pending"
  },
  {
    "id": 9223372036854773000,
    "category": {
      "id": 606,
      "name": "string"
    },
    "name": "bird",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 606,
        "name": "Kesha"
      }
    ],
    "status": "pending"
  },
  {
    "id": 9223372036854773000,
    "category": {
      "id": 606,
      "name": "string"
    },
    "name": "bird",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 606,
        "name": "Kesha"
      }
    ],
    "status": "pending"
  },
  {
    "id": 9223372036854773000,
    "category": {
      "id": 606,
      "name": "string"
    },
    "name": "bird",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 606,
        "name": "Kesha"
      }
    ],
    "status": "pending"
  },
  {
    "id": 9223372036854773000,
    "category": {
      "id": 606,
      "name": "string"
    },
    "name": "bird",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 606,
        "name": "Kesha"
      }
    ],
    "status": "pending"
  },
  {
    "id": 9223372036854773000,
    "category": {
      "id": 606,
      "name": "string"
    },
    "name": "bird",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 606,
        "name": "Kesha"
      }
    ],
    "status": "pending"
  },
  {
    "id": 9223372036854773000,
    "category": {
      "id": -87280417,
      "name": "Excepteur cupidatat laboris ipsum"
    },
    "name": "doggie",
    "photoUrls": [
      "nulla incididunt fugiat cupidatat",
      "quis ipsum consectetur sunt"
    ],
    "tags": [
      {
        "id": -58852255,
        "name": "eu sint deserunt dolore"
      },
      {
        "id": -19810017,
        "name": "ut officia"
      }
    ],
    "status": "pending"
  },
  {
    "id": 9223372036854773000,
    "category": {
      "id": -87280417,
      "name": "Excepteur cupidatat laboris ipsum"
    },
    "name": "doggie",
    "photoUrls": [
      "nulla incididunt fugiat cupidatat",
      "quis ipsum consectetur sunt"
    ],
    "tags": [
      {
        "id": -58852255,
        "name": "eu sint deserunt dolore"
      },
      {
        "id": -19810017,
        "name": "ut officia"
      }
    ],
    "status": "pending"
  },
  {
    "id": 9223372036854773000,
    "category": {
      "id": 19785529,
      "name": "magna qui commodo velit"
    },
    "name": "doggie",
    "photoUrls": [
      "eiusmod elit incididunt",
      "et cupidatat officia sed"
    ],
    "tags": [
      {
        "id": 80293477,
        "name": "labore culpa amet cillum in"
      },
      {
        "id": -69929925,
        "name": "reprehenderit aute veniam exercitation"
      }
    ],
    "status": "pending"
  },
  {
    "id": 9223372036854773000,
    "category": {
      "id": 606,
      "name": "string"
    },
    "name": "bird",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 606,
        "name": "Kesha"
      }
    ],
    "status": "pending"
  },
  {
    "id": 9223372036854773000,
    "category": {
      "id": 81570204,
      "name": "in veniam ad qui in"
    },
    "name": "doggie",
    "photoUrls": [
      "ad dolore",
      "aliquip enim a"
    ],
    "tags": [
      {
        "id": 90749008,
        "name": "tempor eiusmod proident"
      },
      {
        "id": -50220980,
        "name": "ea"
      }
    ],
    "status": "pending"
  },
  {
    "id": 9223372036854773000,
    "category": {
      "id": 17993983,
      "name": "exercitation ut eu"
    },
    "name": "doggie",
    "photoUrls": [
      "non reprehenderit ea sed cupidatat",
      "aliqua exercitation irure"
    ],
    "tags": [
      {
        "id": 12090502,
        "name": "Duis ex"
      },
      {
        "id": -71103809,
        "name": "laborum nulla"
      }
    ],
    "status": "pending"
  },
  {
    "id": 68783552,
    "category": {
      "id": 57019602,
      "name": "in"
    },
    "name": "doggie",
    "photoUrls": [
      "fugiat sunt occaecat labore Excepteur",
      "cupidatat in"
    ],
    "tags": [
      {
        "id": 41094418,
        "name": "ut Duis"
      },
      {
        "id": -96713650,
        "name": "ut velit exercitation cillum"
      }
    ],
    "status": "pending"
  },
  {
    "id": 9223372036854773000,
    "category": {
      "id": 20820300,
      "name": "Praim"
    },
    "name": "Dogs",
    "photoUrls": [
      "veniam",
      "https://animaljournal.ru/articles/pets/dogs/angliyskiy_koker_spaniel/angliyskiy_koker_spaniel_uhod.jpg"
    ],
    "tags": [
      {
        "id": -86399966,
        "name": "ullamco consequat"
      },
      {
        "id": 78422311,
        "name": "aute voluptate"
      }
    ],
    "status": "pending"
  },
  {
    "id": 9223372036854773000,
    "category": {
      "id": 606,
      "name": "string"
    },
    "name": "bird",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 606,
        "name": "Kesha"
      }
    ],
    "status": "pending"
  },
  {
    "id": 9223372036854773000,
    "category": {
      "id": 606,
      "name": "string"
    },
    "name": "bird",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 606,
        "name": "Kesha"
      }
    ],
    "status": "pending"
  },
  {
    "id": 9223372036854773000,
    "category": {
      "id": 606,
      "name": "string"
    },
    "name": "bird",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 606,
        "name": "Kesha"
      }
    ],
    "status": "pending"
  },
  {
    "id": 9223372036854773000,
    "category": {
      "id": 606,
      "name": "string"
    },
    "name": "bird",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 606,
        "name": "Kesha"
      }
    ],
    "status": "pending"
  },
  {
    "id": 9223372036854773000,
    "category": {
      "id": 606,
      "name": "string"
    },
    "name": "bird",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 606,
        "name": "Kesha"
      }
    ],
    "status": "pending"
  },
  {
    "id": 9223372036854773000,
    "category": {
      "id": 606,
      "name": "string"
    },
    "name": "bird",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 606,
        "name": "Kesha"
      }
    ],
    "status": "pending"
  },
  {
    "id": 9223372036854773000,
    "category": {
      "id": 606,
      "name": "string"
    },
    "name": "bird",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 606,
        "name": "Kesha"
      }
    ],
    "status": "pending"
  },
  {
    "id": 9223372036854773000,
    "category": {
      "id": 606,
      "name": "string"
    },
    "name": "bird",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 606,
        "name": "Kesha"
      }
    ],
    "status": "pending"
  },
  {
    "id": 9223372036854773000,
    "category": {
      "id": 606,
      "name": "string"
    },
    "name": "bird",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 606,
        "name": "Kesha"
      }
    ],
    "status": "pending"
  },
  {
    "id": 9223372036854773000,
    "category": {
      "id": 606,
      "name": "string"
    },
    "name": "bird",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 606,
        "name": "Kesha"
      }
    ],
    "status": "pending"
  },
  {
    "id": 9223372036854773000,
    "category": {
      "id": 606,
      "name": "string"
    },
    "name": "bird",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 606,
        "name": "Kesha"
      }
    ],
    "status": "pending"
  },
  {
    "id": 9223372036854773000,
    "category": {
      "id": 606,
      "name": "string"
    },
    "name": "bird",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 606,
        "name": "Kesha"
      }
    ],
    "status": "pending"
  },
  {
    "id": 9223372036854773000,
    "category": {
      "id": 606,
      "name": "string"
    },
    "name": "bird",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 606,
        "name": "Kesha"
      }
    ],
    "status": "pending"
  },
  {
    "id": 17713,
    "category": {
      "id": 606,
      "name": "string"
    },
    "name": "bird",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 17713,
        "name": "Stesha"
      }
    ],
    "status": "pending"
  },
  {
    "id": 9223372036854773000,
    "category": {
      "id": 606,
      "name": "string"
    },
    "name": "bird",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 606,
        "name": "Kesha"
      }
    ],
    "status": "pending"
  },
  {
    "id": 9223372036854773000,
    "category": {
      "id": -82420020,
      "name": "magna"
    },
    "name": "doggie",
    "photoUrls": [
      "ullamco cillum commodo",
      "reprehenderit mollit l"
    ],
    "tags": [
      {
        "id": -15773873,
        "name": "sit ut cupidatat adipisic"
      },
      {
        "id": 10692263,
        "name": "consequat in"
      }
    ],
    "status": "pending"
  },
  {
    "id": 12345,
    "category": {
      "id": 0,
      "name": "Tuto-Karate2"
    },
    "name": "doggie",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 0,
        "name": "string"
      }
    ],
    "status": "pending"
  }
]
`

func BenchmarkPets_Marshals(b *testing.B) {
	var (
		pets Pets
		err  error
		data []byte
	)
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		pets, err = UnmarshalPets([]byte(jsonData))
		if err != nil {
			panic(err)

		}
		data, err = pets.Marshal()
		if err != nil {
			panic(err)
		}

		_ = data

	}
}

func BenchmarkPets_Marshals2(b *testing.B) {
	var (
		pets Pets
		err  error
		data []byte
	)
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		pets, err = UnmarshalPets2([]byte(jsonData))
		if err != nil {
			panic(err)

		}
		data, err = pets.Marshal2()
		if err != nil {
			panic(err)
		}

		_ = data

	}
}
