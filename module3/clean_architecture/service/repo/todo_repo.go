package repo

import (
	"encoding/json"
	"os"

	"gitlab.com/zapirus/gokata/module3/clean_architecture/service/models"
)

// TaskRepository is a repository interface for tasks
type TaskRepository interface {
	GetTasks(d *models.Todo) ([]models.Todo, error)
	GetTask(id int) (models.Todo, error)
	CreateTask(task models.Todo) (models.Todo, error)
	UpdateTask(task models.Todo) (models.Todo, error)
	DeleteTask(id int) error
}

// FileTaskRepository is a file-based implementation of TaskRepository
type FileTaskRepository struct {
	FilePath string
}

func (repo *FileTaskRepository) DeleteTask(id int) error {
	//TODO implement me
	panic("implement me")
}

func NewUserRepository(file string) *FileTaskRepository {
	return &FileTaskRepository{
		FilePath: file,
	}
}

// GetTasks returns all tasks from the repository
func (repo *FileTaskRepository) GetTasks(d *models.Todo) ([]models.Todo, error) {

	val, _ := os.ReadFile(repo.FilePath)
	var todos []models.Todo
	json.Unmarshal(val, &todos)

	todos = append(todos, *d)

	marshal, _ := json.MarshalIndent(todos, "\t", " ")

	os.WriteFile(repo.FilePath, marshal, 0644)

	return todos, nil
}

// GetTask returns a single task by its ID
func (repo *FileTaskRepository) GetTask(id int) (models.Todo, error) {
	var task models.Todo

	todos, err := repo.GetTasks(&models.Todo{})
	if err != nil {
		return task, err
	}

	for _, t := range todos {
		if t.ID == id {
			return t, nil
		}
	}

	return task, nil
}

// CreateTask adds a new task to the repository
func (repo *FileTaskRepository) CreateTask(todo models.Todo) (models.Todo, error) {
	todos, err := repo.GetTasks(&todo)
	if err != nil {
		return todo, err
	}

	todo.ID = len(todos) + 1
	todos = append(todos, todo)

	return todo, nil
}

// UpdateTask updates an existing task in the repository
func (repo *FileTaskRepository) UpdateTask(todo models.Todo) (models.Todo, error) {
	todos, err := repo.GetTasks(&models.Todo{})
	if err != nil {
		return todo, err
	}

	for i, t := range todos {
		if t.ID == todo.ID {
			todos[i] = todo
			break
		}
	}

	return todo, nil
}
