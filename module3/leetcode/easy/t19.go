package main

import (
	"fmt"
	"sort"
)

func main() {

	fmt.Println(minimumSum(2932))

}

func minimumSum(num int) int {
	var nums []int
	for num > 0 {
		nums = append(nums, num%10)
		num = num / 10
	}
	sort.Ints(nums)
	return nums[0]*10 + nums[2] + nums[1]*10 + nums[3]

}
