package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
)

var (
	configPath string
)

type Config struct {
	AppName    string `json:"appName"`
	Production bool   `json:"production"`
}

func init() {
	flag.StringVar(&configPath, "configs-path", "module2/stl/flag/homework/configs.json", "")
}
func main() {
	flag.Parse()
	jsonFile, err := os.Open("module2/stl/flag/homework/configs.json")
	if err != nil {
		fmt.Println(err)
	}
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)
	conf := Config{}
	json.Unmarshal(byteValue, &conf)
	indent, _ := json.MarshalIndent(conf, "", " ")
	fmt.Println(string(indent))

}

// Пример вывода конфига
// Production: true
// AppName: мое тестовое приложение
