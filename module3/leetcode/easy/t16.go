package main

import "fmt"

func main() {
	fmt.Println(smallestEvenMultiple(77))

}

func smallestEvenMultiple(n int) int {
	val := 0

	for i := 1; i <= 1111; i++ {
		if i%2 == 0 && i%n == 0 {
			val = i
			break
		}
	}
	return val

}
