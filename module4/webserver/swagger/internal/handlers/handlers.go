package handlers

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"gitlab.com/zapirus/gokata/module4/webserver/swagger/internal/repository"
	"gitlab.com/zapirus/gokata/module4/webserver/swagger/internal/serviceswag"

	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/chi/v5"
)

func NewPetController() serviceswag.PetController { // конструктор нашего контроллера
	return serviceswag.PetController{
		Storage: *repository.NewPetStorage(),
		Store:   *repository.NewOrderStorage(),
		User:    *repository.NewUserStorage(),
	}
}

func Run() {

	port := ":8080"

	r := chi.NewRouter()
	r.Use(middleware.Logger)

	petController := NewPetController()
	r.Post("/pet", petController.PetCreate)
	r.Get("/pet/{petID}", petController.PetGetByID)
	r.Post("/pet{petID}/uploadImg", petController.PetGetByID)
	r.Put("/pet", petController.Put)
	r.Post("/pet{petID}", petController.UpdatePet)
	r.Delete("/pet{petID}", petController.DeletePet)

	//Store routs
	r.Post("/store/order", petController.CreateOrder)
	r.Get("/store/order/{orderID}", petController.GetOrder)
	r.Delete("/store/order/{orderID}", petController.DeleteOrder)
	r.Get("/store/inventory", petController.GetInventory)

	//User routs
	r.Post("/user", petController.CreateUser)
	r.Get("/user/{username}", petController.GetUser)
	r.Put("/user/{username}", petController.PutUser)
	r.Delete("/store/{username}", petController.DeleteUser)

	//SwaggerUI
	r.Get("/swagger", serviceswag.SwaggerUI)
	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("/home/huawei/src/gitlab.com/zapirus/gokata/module4/webserver/swagger/public"))).ServeHTTP(w, r)
	})

	srv := &http.Server{
		Addr:    port,
		Handler: r,
	}

	// Запуск веб-сервера в отдельном горутине
	go func() {
		log.Println(fmt.Sprintf("server started on port %s ", port))
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// Ожидание сигнала для начала завершения работы
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")

	// Установка тайм-аута для завершения работы
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}

	log.Println("Server exiting")
}
