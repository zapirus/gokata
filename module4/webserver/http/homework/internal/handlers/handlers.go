package handlers

import (
	"context"
	"errors"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/zapirus/gokata/module4/webserver/http/homework/internal/service"

	"github.com/go-chi/chi/v5"
	"github.com/sirupsen/logrus"
	"gitlab.com/zapirus/gokata/module4/webserver/http/homework/internal/config"
)

type APIServer struct {
	conf   *config.Config
	router chi.Router
	logger *logrus.Logger
	serv   *service.MethodsService
}

func New(config *config.Config) *APIServer {
	return &APIServer{
		conf:   config,
		router: chi.NewRouter(),
		logger: logrus.New(),
	}
}

func (s *APIServer) Run() {
	srv := &http.Server{
		Addr:    s.conf.HTTPAddr,
		Handler: s.router,
	}

	s.confRouter()
	s.logger.Printf("Завелись на порту %s", s.conf.HTTPAddr)
	idConnClosed := make(chan struct{})
	go func() {
		sigint := make(chan os.Signal, 1)
		signal.Notify(sigint, os.Interrupt, syscall.SIGTERM)
		<-sigint
		ctx, cancel := context.WithTimeout(context.Background(), 4*time.Second)
		defer cancel()
		if err := srv.Shutdown(ctx); err != nil {
			s.logger.Fatalln(err)
		}
		close(idConnClosed)
	}()
	if err := srv.ListenAndServe(); err != nil {
		if !errors.Is(err, http.ErrServerClosed) {
			s.logger.Fatalln(err)
		}
	}
	<-idConnClosed
	s.logger.Println("Пока-пока!!!")
}

// Роуты
func (s *APIServer) confRouter() {
	s.router.Get("/", s.serv.Hello())
	s.router.HandleFunc("/users", s.serv.GetUsers())
	s.router.Get("/users/{id}", s.serv.GetUser())
	s.router.Post("/uploads", s.serv.UploadFile())
	s.router.Get("/getnames", s.serv.GetNames())
}
