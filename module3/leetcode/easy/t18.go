package main

import "fmt"

func main() {
	arr := []int{1, 15, 6, 3}

	fmt.Println(differenceOfSum(arr))

}
func differenceOfSum(nums []int) int {
	var val int

	for i := 0; i < len(nums); i++ {
		if nums[i] < 10 {
			val += nums[i]
		} else if nums[i] > 9 && nums[i] < 100 {
			rs := nums[i] % 10
			rs2 := nums[i] / 10
			val = val + rs + rs2

		} else if nums[i] > 99 && nums[i] < 1000 {
			rs := nums[i] / 10 % 10
			rs2 := nums[i] % 10
			rs3 := nums[i] / 100
			val = val + rs + rs2 + rs3

		} else if nums[i] > 999 && nums[i] < 10000 {
			rs := nums[i] / 10 % 10
			rs2 := nums[i] % 10
			rs3 := nums[i] / 100 % 10
			rs4 := nums[i] / 100 / 10
			val = val + rs + rs2 + rs3 + rs4
		}
	}
	res2 := 0

	for _, num := range nums {
		res2 += num
	}

	return res2 - val
}
